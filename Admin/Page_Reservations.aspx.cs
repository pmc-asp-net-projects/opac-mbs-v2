using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Page_Reservations : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        SqlDataSource1.SelectCommand = "SELECT Reservations.ReserveNo, Vehicles.InventoryNo, Vehicles.ChassisNo, Vehicles.BodyType, Vehicles.EngineNo, Vehicles.HorsePower, Vehicles.GasCapacity, Vehicles.ImagePath, Vehicles.Price FROM Reservations INNER JOIN Vehicles ON Reservations.InventoryNo = Vehicles.InventoryNo WHERE [OrderNo] IS NULL";
        SqlDataSource1.Select(DataSourceSelectArguments.Empty);

        SqlDataSource3.SelectCommand = "SELECT SUM(Vehicles.Price) AS TotalCost FROM Reservations INNER JOIN Vehicles ON Reservations.InventoryNo = Vehicles.InventoryNo WHERE [OrderNo] IS NULL";
        SqlDataSource3.Select(DataSourceSelectArguments.Empty);

        SqlDataSource4.SelectCommand = "SELECT COUNT(*) AS TotalItems FROM Reservations  WHERE [OrderNo] IS NULL";
        SqlDataSource4.Select(DataSourceSelectArguments.Empty);

        SqlDataSource5.SelectCommand = "SELECT aspnet_Users.UserName, ExtraInfo.Address FROM aspnet_Users INNER JOIN ExtraInfo ON aspnet_Users.UserId = ExtraInfo.UserID  WHERE [UserName] = '" + User.Identity.Name.ToString() + "'";
        SqlDataSource5.Select(DataSourceSelectArguments.Empty);

        if (GridView3.Rows[0].Cells[0].Text == "0")
        {
            Label1.Visible = false;
            RadioButton1.Visible = false;
            RadioButton2.Visible = false;
            RadioButton3.Visible = false;
            Button1.Visible = false;

            Label2.Visible = false;
            RadioButtonList1.Visible = false;
        }
        else
        {
            Label1.Visible = true;
            RadioButton1.Visible = true;
            RadioButton2.Visible = true;
            RadioButton3.Visible = true;
            Button1.Visible = true;

            Label2.Visible = true;
            RadioButtonList1.Visible = true;
        }


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int temp;
        string temp2 = "";

        if (RadioButton1.Checked == true)
        {
            temp2 = "CASH";
        }
        else if (RadioButton2.Checked == true)
        {
            temp2 = "DEPOSIT";
        }
        else if (RadioButton3.Checked == true)
        {
            temp2 = "CHECK";
        }

        if (LastRecord(SqlDataSource2, "SELECT COUNT(*) AS [Total] FROM [Orders] WHERE [Customer] = '" + User.Identity.Name.ToString() + "' AND [Status] <> 'FINISHED'") == 0)
        {

            SqlDataSource2.InsertCommand = "INSERT INTO [Orders] ([Customer], [DateOrdered], [Status], [TotalCost], [MethodOfPayment], [MonthsToPay]) VALUES ('" + User.Identity.Name.ToString() + "', " + DateTime.Today.ToShortDateString() + ", 'UNCONFIRMED', '" + GridView2.Rows[0].Cells[0].Text + "', '" + temp2 + "', '" + RadioButtonList1.SelectedValue + "')";
            SqlDataSource2.Insert();

            temp = LastRecord(SqlDataSource2, "SELECT TOP 1 [OrderNo] AS [Total] FROM [Orders] ORDER BY [OrderNo] DESC");

            SqlDataSource1.UpdateCommand = "UPDATE [Reservations] SET [OrderNo] = " + temp + " WHERE [OrderNo] IS NULL AND [Customer] = '" + User.Identity.Name.ToString() + "'";
            SqlDataSource1.Update();

            ErrorMessageLabel.Visible = false;

            Server.Transfer("Page_OrdersList.aspx");
        }
        else
        {
            ErrorMessageLabel.Visible = true;
        }
    }

    public static int LastRecord(SqlDataSource datasource, string SearchString03)
    {

        datasource.SelectCommand = SearchString03;
        System.Data.DataView dv = ((System.Data.DataView)(datasource.Select(DataSourceSelectArguments.Empty)));

        return (Int32.Parse(dv.Table.Rows[0]["Total"].ToString()));
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlDataSource1.DeleteCommand = "DELETE FROM [Reservations] WHERE [ReserveNo] = " + GridView1.SelectedRow.Cells[0].Text;
        SqlDataSource1.Delete();
    }
}
