using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Page_Contractor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        int Number;

        if (Request["ContractorNumber"] == null)
        {
            if (TextBox1.Text == "") TextBox1.Text = "0";
            Number = int.Parse(TextBox1.Text);
        }
        else
        {
            TextBox1.Text = Request["ContractorNumber"];
            Number = int.Parse(Request["ContractorNumber"]);
        }
        
        SqlDataSource1.SelectCommand = "SELECT * FROM [Vehicles] WHERE [ContractorNumber] = '" + Number + "'";
        SqlDataSource1.Select(DataSourceSelectArguments.Empty);

        SqlDataSource2.SelectCommand = "SELECT * FROM [ContractorTable] WHERE [ContractorNumber] = '" + Number + "'";
        SqlDataSource2.Select(DataSourceSelectArguments.Empty);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlDataSource1.SelectCommand = "SELECT * FROM [Vehicles] WHERE [ContractorNumber] = '" + TextBox1.Text + "'";
        SqlDataSource1.Select(DataSourceSelectArguments.Empty);

        SqlDataSource2.SelectCommand = "SELECT * FROM [ContractorTable] WHERE [ContractorNumber] = '" + TextBox1.Text + "'";
        SqlDataSource2.Select(DataSourceSelectArguments.Empty);
    }
}
