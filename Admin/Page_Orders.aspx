<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Orders.aspx.cs" Inherits="Admin_Page_Orders" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    <br />
    <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="12pt" NavigateUrl="Page_Reservations.aspx">Cart</asp:HyperLink><span
        style="font-size: 12pt"> |
        <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="12pt" NavigateUrl="~/Admin/Page_OrdersList.aspx">Order List</asp:HyperLink>
        |
        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Admin/Page_Orders.aspx">Order Details</asp:HyperLink></span><span
        style="font-size: 12pt"></span><br />
    <br />
    <span style="font-size: 16pt"><strong>Orders<br />
    </strong></span>&nbsp;</center>
    <center>
        <span style="font-size: 12pt">Order Number:</span>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Search" /></center>
    <center>
        &nbsp;</center>
    <center>
        <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True"
        AutoGenerateRows="False" CellPadding="4" DataSourceID="SqlDataSource1"
        ForeColor="#333333" GridLines="None" Height="50px" Width="125px" Font-Size="9pt" DataKeyNames="OrderNo">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
        <EditRowStyle BackColor="#7C6F57" />
        <RowStyle BackColor="#E3EAEB" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <Fields>
            <asp:BoundField DataField="OrderNo" HeaderText="Order #" InsertVisible="False" ReadOnly="True"
                SortExpression="OrderNo" />
            <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" />
            <asp:BoundField DataField="DateOrdered" HeaderText="Date Ordered" SortExpression="DateOrdered" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" SortExpression="TotalCost" />
            <asp:BoundField DataField="MethodOfPayment" HeaderText="Method Of Payment" SortExpression="MethodOfPayment" />
            <asp:BoundField DataField="MonthsToPay" HeaderText="Months To Pay" SortExpression="MonthsToPay" />
        </Fields>
        <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:DetailsView>
    &nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [Orders] WHERE [OrderNo] = @OrderNo" InsertCommand="INSERT INTO [Orders] ([Customer], [DateOrdered], [Status], [TotalCost], [MethodOfPayment]) VALUES (@Customer, @DateOrdered, @Status, @TotalCost, @MethodOfPayment)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT * FROM [Orders]"
        UpdateCommand="UPDATE [Orders] SET [Customer] = @Customer, [DateOrdered] = @DateOrdered, [Status] = @Status, [TotalCost] = @TotalCost, [MethodOfPayment] = @MethodOfPayment WHERE [OrderNo] = @OrderNo">
        <DeleteParameters>
            <asp:Parameter Name="OrderNo" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Customer" Type="String" />
            <asp:Parameter Name="DateOrdered" Type="DateTime" />
            <asp:Parameter Name="Status" Type="String" />
            <asp:Parameter Name="TotalCost" Type="Decimal" />
            <asp:Parameter Name="MethodOfPayment" Type="String" />
            <asp:Parameter Name="OrderNo" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="Customer" Type="String" />
            <asp:Parameter Name="DateOrdered" Type="DateTime" />
            <asp:Parameter Name="Status" Type="String" />
            <asp:Parameter Name="TotalCost" Type="Decimal" />
            <asp:Parameter Name="MethodOfPayment" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
    &nbsp;&nbsp;<br />
    <br />
    <span style="font-size: 16pt"><strong>Item(s) Ordered:</strong></span><br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource2" EmptyDataText="There are no data records to display."
            ForeColor="#333333" GridLines="Vertical">
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="InventoryNo" HeaderText="Inventory #" InsertVisible="False"
                    ReadOnly="True" SortExpression="InventoryNo" />
                <asp:ImageField DataImageUrlField="ImagePath">
                </asp:ImageField>
                <asp:BoundField DataField="BodyType" HeaderText="Body Type" SortExpression="BodyType" />
                <asp:BoundField DataField="ChassisNo" HeaderText="Chassis #" SortExpression="ChassisNo" />
                <asp:BoundField DataField="EngineNo" HeaderText="Engine #" SortExpression="EngineNo" />
                <asp:BoundField DataField="HorsePower" HeaderText="Horse Power" SortExpression="HorsePower" />
                <asp:BoundField DataField="GasCapacity" HeaderText="Gas Capacity" SortExpression="GasCapacity" />
                <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            </Columns>
            <RowStyle BackColor="#E3EAEB" />
            <EditRowStyle BackColor="#7C6F57" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
            DeleteCommand="DELETE FROM [Reservations] WHERE [ReserveNo] = @ReserveNo" InsertCommand="INSERT INTO [Reservations] ([Customer], [InventoryNo], [DateReserved], [Confirmed], [Paid], [Claimed], [OrderNo]) VALUES (@Customer, @InventoryNo, @DateReserved, @Confirmed, @Paid, @Claimed, @OrderNo)"
            ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
            SelectCommand="SELECT Reservations.ReserveNo, Reservations.OrderNo, Vehicles.InventoryNo, Vehicles.BodyType, Vehicles.ChassisNo, Vehicles.EngineNo, Vehicles.HorsePower, Vehicles.GasCapacity, Vehicles.ImagePath, Vehicles.Price FROM Reservations INNER JOIN Vehicles ON Reservations.InventoryNo = Vehicles.InventoryNo"
            UpdateCommand="UPDATE [Reservations] SET [Customer] = @Customer, [InventoryNo] = @InventoryNo, [DateReserved] = @DateReserved, [Confirmed] = @Confirmed, [Paid] = @Paid, [Claimed] = @Claimed, [OrderNo] = @OrderNo WHERE [ReserveNo] = @ReserveNo">
            <DeleteParameters>
                <asp:Parameter Name="ReserveNo" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Customer" Type="String" />
                <asp:Parameter Name="InventoryNo" Type="Int32" />
                <asp:Parameter Name="DateReserved" Type="DateTime" />
                <asp:Parameter Name="Confirmed" Type="Boolean" />
                <asp:Parameter Name="Paid" Type="Boolean" />
                <asp:Parameter Name="Claimed" Type="Boolean" />
                <asp:Parameter Name="OrderNo" Type="Int32" />
                <asp:Parameter Name="ReserveNo" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Customer" Type="String" />
                <asp:Parameter Name="InventoryNo" Type="Int32" />
                <asp:Parameter Name="DateReserved" Type="DateTime" />
                <asp:Parameter Name="Confirmed" Type="Boolean" />
                <asp:Parameter Name="Paid" Type="Boolean" />
                <asp:Parameter Name="Claimed" Type="Boolean" />
                <asp:Parameter Name="OrderNo" Type="Int32" />
            </InsertParameters>
        </asp:SqlDataSource>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    </center>
</asp:Content>

