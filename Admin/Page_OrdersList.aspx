<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_OrdersList.aspx.cs" Inherits="Admin_Page_OrdersList" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    <br />
    <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="12pt" NavigateUrl="Page_Reservations.aspx">Cart</asp:HyperLink><span
        style="font-size: 12pt"> |
        <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="12pt" NavigateUrl="~/Admin/Page_OrdersList.aspx">Order List</asp:HyperLink>
        |
        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Admin/Page_Orders.aspx">Order Details</asp:HyperLink></span><span></span><br />
    <br />
    <span style="font-size: 16pt"><strong>Orders</strong></span></center>
    <center>
        &nbsp;</center>
    <center>
        <span style="font-size: 12pt">View By: </span>
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            <asp:ListItem>All</asp:ListItem>
            <asp:ListItem>Pending</asp:ListItem>
            <asp:ListItem>Finished</asp:ListItem>
        </asp:DropDownList><span style="font-size: 16pt"><strong><br />
        </strong></span>&nbsp;<br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="OrderNo" DataSourceID="SqlDataSource1"
        EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="Vertical">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="OrderNo" DataNavigateUrlFormatString="Page_Orders.aspx?OrderNo={0}"
                DataTextField="OrderNo" Text="Order Number" HeaderText="Order Number" />
            <asp:HyperLinkField DataNavigateUrlFields="Customer" DataNavigateUrlFormatString="Page_Members.aspx?UserName={0}"
                DataTextField="Customer" HeaderText="Customer" Text="Customer" />
            <asp:BoundField DataField="DateOrdered" HeaderText="Date Ordered" SortExpression="DateOrdered" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" SortExpression="TotalCost" />
            <asp:BoundField DataField="MethodOfPayment" HeaderText="Method Of Payment" SortExpression="MethodOfPayment" />
            <asp:BoundField DataField="MonthsToPay" HeaderText="Months To Pay" SortExpression="MonthsToPay" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
        <RowStyle BackColor="#E3EAEB" />
        <EditRowStyle BackColor="#7C6F57" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [Orders] WHERE [OrderNo] = @OrderNo" InsertCommand="INSERT INTO [Orders] ([Customer], [DateOrdered], [Status], [TotalCost], [MethodOfPayment]) VALUES (@Customer, @DateOrdered, @Status, @TotalCost, @MethodOfPayment)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT * FROM [Orders]"
        UpdateCommand="UPDATE [Orders] SET [DateOrdered] = @DateOrdered, [Status] = @Status, [TotalCost] = @TotalCost, [MethodOfPayment] = @MethodOfPayment WHERE [OrderNo] = @OrderNo">
        <InsertParameters>
            <asp:Parameter Name="Customer" Type="String" />
            <asp:Parameter Name="DateOrdered" Type="DateTime" />
            <asp:Parameter Name="Status" Type="String" />
            <asp:Parameter Name="TotalCost" Type="Decimal" />
            <asp:Parameter Name="MethodOfPayment" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="DateOrdered" Type="DateTime" />
            <asp:Parameter Name="Status" Type="String" />
            <asp:Parameter Name="TotalCost" Type="Decimal" />
            <asp:Parameter Name="MethodOfPayment" Type="String" />
            <asp:Parameter Name="OrderNo" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="OrderNo" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    </center>
</asp:Content>

