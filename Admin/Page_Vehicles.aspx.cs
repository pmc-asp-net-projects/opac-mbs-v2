using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Page_Vehicles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        UpdateItems();
    }

    private void UpdateItems()
    {
        int Number;

        if (TextBox1.Text == "")
        {
            if (Request["VehicleNumber"] == null) TextBox1.Text = "1";
            else TextBox1.Text = Request["VehicleNumber"];
        }

        Number = int.Parse(TextBox1.Text);

        Label1.Text = Convert.ToString(RecordsChecker2(SqlDataSource2, "SELECT * FROM [Vehicles]"));
        Button3.Enabled = RecordsChecker(SqlDataSource2, "SELECT * FROM [Vehicles] WHERE [InventoryNo] = " + (Number - 1));
        Button4.Enabled = RecordsChecker(SqlDataSource2, "SELECT * FROM [Vehicles] WHERE [InventoryNo] = " + (Number + 1));

        if (RecordsChecker(SqlDataSource2, "SELECT * FROM [Vehicles] WHERE [InventoryNo] = " + Number) == true)
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM [Vehicles] WHERE [InventoryNo] = " + Number;
            SqlDataSource1.Select(DataSourceSelectArguments.Empty);

            SqlDataSource3.SelectCommand = "SELECT * FROM [CommentTable] WHERE [Reciever] = '" + Number + "' ORDER BY [CommentNo] DESC";
            SqlDataSource3.Select(DataSourceSelectArguments.Empty);

            DetailsView1.DataBind();
            GridView1.DataBind();

            ErrorMessageLabel.Visible = false;
        }
        else
        {
            ErrorMessageLabel.Visible = true;
        }

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
    }
 
    protected void Button2_Click(object sender, EventArgs e)
    {
        SqlDataSource3.InsertCommand = "INSERT INTO [CommentTable] ([PostDate], [Sender], [Reciever], [Message]) VALUES ('" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "', '" + User.Identity.Name.ToString() + "', '" + TextBox1.Text + "', '" + TextBox2.Text + "')";
        SqlDataSource3.Insert();

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        TextBox1.Text = Convert.ToString(int.Parse(TextBox1.Text) - 1);
        UpdateItems();
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        TextBox1.Text = Convert.ToString(int.Parse(TextBox1.Text) + 1);
        UpdateItems();
    }


    public static bool RecordsChecker(SqlDataSource datasource, string SearchString03)
    {
        datasource.SelectCommand = SearchString03;
        System.Data.DataView dv = ((System.Data.DataView)(datasource.Select(DataSourceSelectArguments.Empty)));

        if (dv.Table.Rows.Count != 0) { return true; }
        else { return false; }

    }

    public static int RecordsChecker2(SqlDataSource datasource, string SearchString03)
    {
        datasource.SelectCommand = SearchString03;
        System.Data.DataView dv = ((System.Data.DataView)(datasource.Select(DataSourceSelectArguments.Empty)));

        return dv.Table.Rows.Count;
    }
}
