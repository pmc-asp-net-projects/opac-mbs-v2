<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_NewEntry.aspx.cs" Inherits="Admin_Page_NewEntry" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    &nbsp;<br />
    <strong><span style="font-size: 16pt">Add New Entry<br />
    </span></strong>&nbsp;<br />
    <strong><span style="font-size: 12pt">Upload File</span></strong><br />
    <br />
    <asp:FileUpload ID="FileUpload1" runat="server" /><br />
    <br />
    <asp:Button ID="Button1" runat="server" Text="Upload" /><br />
    <br />
    <span style="font-size: 12pt"><strong>New Vehicle</strong><br />
    </span>
    <br />
    <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False"
        CellPadding="4" DataKeyNames="InventoryNo" DataSourceID="SqlDataSource1" ForeColor="#333333"
        GridLines="None" Height="50px" Width="125px">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
        <EditRowStyle BackColor="#7C6F57" />
        <RowStyle BackColor="#E3EAEB" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <Fields>
            <asp:BoundField DataField="InventoryNo" HeaderText="Inventory #" InsertVisible="False"
                ReadOnly="True" SortExpression="InventoryNo" />
            <asp:BoundField DataField="ChassisNo" HeaderText="Chassis #" SortExpression="ChassisNo" />
            <asp:BoundField DataField="BodyType" HeaderText="Body Type" SortExpression="BodyType" />
            <asp:BoundField DataField="EngineNo" HeaderText="Engine #" SortExpression="EngineNo" />
            <asp:BoundField DataField="HorsePower" HeaderText="Horse Power" SortExpression="HorsePower" />
            <asp:BoundField DataField="GasCapacity" HeaderText="Gas Capacity" SortExpression="GasCapacity" />
            <asp:BoundField DataField="Features" HeaderText="Features" SortExpression="Features" />
            <asp:BoundField DataField="ImagePath" HeaderText="ImagePath" SortExpression="ImagePath" />
            <asp:BoundField DataField="ContractorNumber" HeaderText="Contractor #" SortExpression="ContractorNumber" />
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
        <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [Vehicles] WHERE [InventoryNo] = @InventoryNo" InsertCommand="INSERT INTO [Vehicles] ([ChassisNo], [BodyType], [EngineNo], [HorsePower], [GasCapacity], [Features], [ImagePath], [ContractorNumber], [Price]) VALUES (@ChassisNo, @BodyType, @EngineNo, @HorsePower, @GasCapacity, @Features, @ImagePath, @ContractorNumber, @Price)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT [InventoryNo], [ChassisNo], [BodyType], [EngineNo], [HorsePower], [GasCapacity], [Features], [ImagePath], [ContractorNumber], [Price] FROM [Vehicles]"
        UpdateCommand="UPDATE [Vehicles] SET [ChassisNo] = @ChassisNo, [BodyType] = @BodyType, [EngineNo] = @EngineNo, [HorsePower] = @HorsePower, [GasCapacity] = @GasCapacity, [Features] = @Features, [ImagePath] = @ImagePath, [ContractorNumber] = @ContractorNumber, [Price] = @Price WHERE [InventoryNo] = @InventoryNo">
        <InsertParameters>
            <asp:Parameter Name="ChassisNo" Type="String" />
            <asp:Parameter Name="BodyType" Type="String" />
            <asp:Parameter Name="EngineNo" Type="String" />
            <asp:Parameter Name="HorsePower" Type="Int32" />
            <asp:Parameter Name="GasCapacity" Type="Int32" />
            <asp:Parameter Name="Features" Type="String" />
            <asp:Parameter Name="ImagePath" Type="String" />
            <asp:Parameter Name="ContractorNumber" Type="Int32" />
            <asp:Parameter Name="Price" Type="Decimal" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ChassisNo" Type="String" />
            <asp:Parameter Name="BodyType" Type="String" />
            <asp:Parameter Name="EngineNo" Type="String" />
            <asp:Parameter Name="HorsePower" Type="Int32" />
            <asp:Parameter Name="GasCapacity" Type="Int32" />
            <asp:Parameter Name="Features" Type="String" />
            <asp:Parameter Name="ImagePath" Type="String" />
            <asp:Parameter Name="ContractorNumber" Type="Int32" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="InventoryNo" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="InventoryNo" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <br />
    <span style="font-size: 12pt"><strong>New Contractor</strong></span><br />
    &nbsp;<asp:DetailsView ID="DetailsView2" runat="server" AllowPaging="True" AutoGenerateRows="False"
        CellPadding="4" DataKeyNames="ContractorNumber" DataSourceID="SqlDataSource2"
        ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
        <EditRowStyle BackColor="#7C6F57" />
        <RowStyle BackColor="#E3EAEB" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <Fields>
            <asp:BoundField DataField="ContractorNumber" HeaderText="Contractor Number" InsertVisible="False"
                ReadOnly="True" SortExpression="ContractorNumber" />
            <asp:BoundField DataField="ContractorName" HeaderText="Contractor Name" SortExpression="ContractorName" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
        <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [ContractorTable] WHERE [ContractorNumber] = @ContractorNumber"
        InsertCommand="INSERT INTO [ContractorTable] ([ContractorName]) VALUES (@ContractorName)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT [ContractorNumber], [ContractorName] FROM [ContractorTable]"
        UpdateCommand="UPDATE [ContractorTable] SET [ContractorName] = @ContractorName WHERE [ContractorNumber] = @ContractorNumber">
        <InsertParameters>
            <asp:Parameter Name="ContractorName" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ContractorName" Type="String" />
            <asp:Parameter Name="ContractorNumber" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="ContractorNumber" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    &nbsp;<br />
    </center>
</asp:Content>

