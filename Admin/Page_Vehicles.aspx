<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Vehicles.aspx.cs" Inherits="Admin_Page_Vehicles" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    <span style="font-size: 16pt">&nbsp;</span>&nbsp;</center>
    <center>
        <span style="font-size: 16pt">
            <asp:Label ID="ErrorMessageLabel" runat="server" ForeColor="Red" Text="Vehicle Number Not Found"
                Visible="False"></asp:Label></span>&nbsp;</center>
    <center>
        <span style="font-size: 16pt"><span style="font-size: 9pt">
            <br />
        </span>
        <strong>Vehicle Details Page</strong></span><strong>&nbsp;<br />
        </strong><span style="font-size: 16pt">
            <br />
            <span style="font-size: 12pt">
                <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="<<" />
                Item:
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                of
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text=">>" /></span></span></center>
    <center>
        <span style="font-size: 16pt"><span style="font-size: 12pt">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Search" /></span><br />
            &nbsp;<br />
        </span>
    </center>
    <center>
                    <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False"
                CellPadding="4" DataKeyNames="InventoryNo" DataSourceID="SqlDataSource1" Font-Size="12pt"
                ForeColor="#333333" GridLines="None" Height="50px" Width="25%">
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                <EditRowStyle BackColor="#7C6F57" />
                <RowStyle BackColor="#E3EAEB" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <Fields>
                    <asp:ImageField DataImageUrlField="ImagePath">
                    </asp:ImageField>
                    <asp:BoundField DataField="InventoryNo" HeaderText="Inventory #" InsertVisible="False"
                        ReadOnly="True" SortExpression="InventoryNo" />
                    <asp:BoundField DataField="ChassisNo" HeaderText="Chassis #" SortExpression="ChassisNo" />
                    <asp:BoundField DataField="BodyType" HeaderText="Body Type" SortExpression="BodyType" />
                    <asp:BoundField DataField="EngineNo" HeaderText="Engine #" SortExpression="EngineNo" />
                    <asp:BoundField DataField="HorsePower" HeaderText="Horse Power" SortExpression="HorsePower" />
                    <asp:BoundField DataField="GasCapacity" HeaderText="Gas Capacity" SortExpression="GasCapacity" />
                    <asp:BoundField DataField="Features" HeaderText="Features" SortExpression="Features" />
                    <asp:BoundField DataField="ContractorNumber" HeaderText="Contractor #" SortExpression="ContractorNumber" />
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                </Fields>
                <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
                <PagerSettings Mode="NumericFirstLast" />
            </asp:DetailsView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
                DeleteCommand="DELETE FROM [Vehicles] WHERE [InventoryNo] = @InventoryNo" InsertCommand="INSERT INTO [Vehicles] ([ChassisNo], [BodyType], [EngineNo], [HorsePower], [GasCapacity], [Features], [ImagePath], [ContractorNumber]) VALUES (@ChassisNo, @BodyType, @EngineNo, @HorsePower, @GasCapacity, @Features, @ImagePath, @ContractorNumber)"
                ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
                SelectCommand="SELECT [InventoryNo], [ChassisNo], [BodyType], [EngineNo], [HorsePower], [GasCapacity], [Features], [ImagePath], [ContractorNumber] FROM [Vehicles]"
                UpdateCommand="UPDATE [Vehicles] SET [ChassisNo] = @ChassisNo, [BodyType] = @BodyType, [EngineNo] = @EngineNo, [HorsePower] = @HorsePower, [GasCapacity] = @GasCapacity, [Features] = @Features, [ImagePath] = @ImagePath, [ContractorNumber] = @ContractorNumber WHERE [InventoryNo] = @InventoryNo">
                <InsertParameters>
                    <asp:Parameter Name="ChassisNo" Type="String" />
                    <asp:Parameter Name="BodyType" Type="String" />
                    <asp:Parameter Name="EngineNo" Type="String" />
                    <asp:Parameter Name="HorsePower" Type="Int32" />
                    <asp:Parameter Name="GasCapacity" Type="Int32" />
                    <asp:Parameter Name="Features" Type="String" />
                    <asp:Parameter Name="ImagePath" Type="String" />
                    <asp:Parameter Name="ContractorNumber" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ChassisNo" Type="String" />
                    <asp:Parameter Name="BodyType" Type="String" />
                    <asp:Parameter Name="EngineNo" Type="String" />
                    <asp:Parameter Name="HorsePower" Type="Int32" />
                    <asp:Parameter Name="GasCapacity" Type="Int32" />
                    <asp:Parameter Name="Features" Type="String" />
                    <asp:Parameter Name="ImagePath" Type="String" />
                    <asp:Parameter Name="ContractorNumber" Type="Int32" />
                    <asp:Parameter Name="InventoryNo" Type="Int32" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:Parameter Name="InventoryNo" Type="Int32" />
                </DeleteParameters>
            </asp:SqlDataSource>
&nbsp;</center>
    <center>
        <strong><span style="font-size: 16pt">Comments</span></strong></center>
    <center>
        <span style="font-size: 16pt">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellPadding="4" DataKeyNames="CommentNo" DataSourceID="SqlDataSource3"
                EmptyDataText="There are no data records to display." Font-Size="10pt" ForeColor="#333333"
                GridLines="Vertical" Width="90%">
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="Sender" HeaderText="Sender" SortExpression="Sender" >
                        <ItemStyle Width="1px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PostDate" HeaderText="Date Time Posted" SortExpression="PostDate" >
                        <ItemStyle Width="1px" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" >
                        <ItemStyle Width="100%" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" >
                        <ItemStyle Width="1px" />
                    </asp:CommandField>
                </Columns>
                <RowStyle BackColor="#E3EAEB" />
                <EditRowStyle BackColor="#7C6F57" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
                DeleteCommand="DELETE FROM [CommentTable] WHERE [CommentNo] = @CommentNo" InsertCommand="INSERT INTO [CommentTable] ([PostDate], [Sender], [Reciever], [Message]) VALUES (@PostDate, @Sender, @Reciever, @Message)"
                ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
                SelectCommand="SELECT [CommentNo], [PostDate], [Sender], [Reciever], [Message] FROM [CommentTable]"
                UpdateCommand="UPDATE [CommentTable] SET [PostDate] = @PostDate, [Sender] = @Sender, [Reciever] = @Reciever, [Message] = @Message WHERE [CommentNo] = @CommentNo">
                <InsertParameters>
                    <asp:Parameter Name="PostDate" Type="DateTime" />
                    <asp:Parameter Name="Sender" Type="String" />
                    <asp:Parameter Name="Reciever" Type="String" />
                    <asp:Parameter Name="Message" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="PostDate" Type="DateTime" />
                    <asp:Parameter Name="Sender" Type="String" />
                    <asp:Parameter Name="Reciever" Type="String" />
                    <asp:Parameter Name="Message" Type="String" />
                    <asp:Parameter Name="CommentNo" Type="Int32" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:Parameter Name="CommentNo" Type="Int32" />
                </DeleteParameters>
            </asp:SqlDataSource>
            &nbsp;&nbsp;<br />
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
                ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
                SelectCommand="SELECT [CommentNo], [PostDate], [Sender], [Reciever], [Message] FROM [CommentTable]">
            </asp:SqlDataSource>
            <span style="font-size: 12pt">Post you comments here:<br />
            </span>
            <asp:TextBox ID="TextBox2" runat="server" Height="64px" Width="75%" TextMode="MultiLine"></asp:TextBox><br />
            <br />
            <asp:Button ID="Button2" runat="server" Text="Post" OnClick="Button2_Click" /></span></center>
</asp:Content>

