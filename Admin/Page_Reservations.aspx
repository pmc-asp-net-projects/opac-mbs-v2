<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Reservations.aspx.cs" Inherits="Page_Reservations" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
    <span style="font-size: 12pt">&nbsp;&nbsp;</span></center>
    <center>
        <span></span><span>
            <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="12pt" NavigateUrl="Page_Reservations.aspx">Cart</asp:HyperLink><span
                style="font-size: 12pt"> |
                <asp:HyperLink ID="HyperLink2" runat="server" Font-Size="12pt" NavigateUrl="~/Admin/Page_OrdersList.aspx">Order List</asp:HyperLink>
                |
                <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Admin/Page_Orders.aspx">Order Details</asp:HyperLink></span></span></center>
    <center>
        <span><span>&nbsp;</span></span><span>&nbsp;</span>&nbsp;</center>
    <center>
        <span style="font-size: 16pt">&nbsp;
            <asp:Label ID="ErrorMessageLabel" runat="server" Text="You Have Still A Pending Order" ForeColor="Red" Visible="False"></asp:Label></span>&nbsp;</center>
    <center>
        <span style="font-size: 16pt">&nbsp;</span><span><br />
            <span style="font-size: 16pt">
            Cart</span></span></center>
    <center>
        <span style="font-size: 16pt">&nbsp;</span><span><span style="font-size: 16pt"></span></span>&nbsp;</center>
    <center>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1"
        EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="Vertical" Width="90%" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="ReserveNo" HeaderText="Cart #" InsertVisible="False"
                ReadOnly="True" SortExpression="ReserveNo" />
            <asp:BoundField DataField="InventoryNo" HeaderText="Inventory #" InsertVisible="False"
                ReadOnly="True" SortExpression="InventoryNo" />
            <asp:ImageField DataImageUrlField="ImagePath">
            </asp:ImageField>
            <asp:BoundField DataField="ChassisNo" HeaderText="Chassis #" SortExpression="ChassisNo" />
            <asp:BoundField DataField="BodyType" HeaderText="Body Type" SortExpression="BodyType" />
            <asp:BoundField DataField="EngineNo" HeaderText="Engine #" SortExpression="EngineNo" />
            <asp:BoundField DataField="HorsePower" HeaderText="Horse Power" SortExpression="HorsePower" />
            <asp:BoundField DataField="GasCapacity" HeaderText="Gas Capacity" SortExpression="GasCapacity" />
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            <asp:CommandField SelectText="Delete" ShowSelectButton="True" />
        </Columns>
        <RowStyle BackColor="#E3EAEB" />
        <EditRowStyle BackColor="#7C6F57" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
        &nbsp;</center>
    <center>
        <span style="font-size: 16pt"></span>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [Reservations] WHERE [ReserveNo] = @ReserveNo" InsertCommand="INSERT INTO [Reservations] ([Customer], [InventoryNo], [DateReserved], [Confirmed], [Paid]) VALUES (@Customer, @InventoryNo, @DateReserved, @Confirmed, @Paid)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT Reservations.ReserveNo, Vehicles.InventoryNo, Vehicles.ChassisNo, Vehicles.BodyType, Vehicles.EngineNo, Vehicles.HorsePower, Vehicles.GasCapacity, Vehicles.ImagePath, Vehicles.Price FROM Reservations INNER JOIN Vehicles ON Reservations.InventoryNo = Vehicles.InventoryNo"
        UpdateCommand="UPDATE Reservations SET Customer = @Customer, InventoryNo = @InventoryNo, DateReserved = @DateReserved, Confirmed = @Confirmed, Paid = @Paid, Claimed = @Claimed WHERE (ReserveNo = @ReserveNo)">
        <DeleteParameters>
            <asp:Parameter Name="ReserveNo" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Customer" Type="String" />
            <asp:Parameter Name="InventoryNo" Type="Int32" />
            <asp:Parameter Name="DateReserved" Type="DateTime" />
            <asp:Parameter Name="Confirmed" Type="Boolean" />
            <asp:Parameter Name="Paid" Type="Boolean" />
            <asp:Parameter Name="Claimed" />
            <asp:Parameter Name="ReserveNo" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="Customer" Type="String" />
            <asp:Parameter Name="InventoryNo" Type="Int32" />
            <asp:Parameter Name="DateReserved" Type="DateTime" />
            <asp:Parameter Name="Confirmed" Type="Boolean" />
            <asp:Parameter Name="Paid" Type="Boolean" />
        </InsertParameters>
    </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
            SelectCommand="SELECT * FROM [Orders]"></asp:SqlDataSource></center>
    <span style="font-size: 16pt"><center>
        More Information:</center>
    </span><span style="font-size: 16pt">
        <center>
            &nbsp;</center>
    </span>
    <center>
        <span style="font-size: 16pt">
            <table style="width: 227px">
                <tr>
                    <td>
                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            DataSourceID="SqlDataSource3" EmptyDataText="There are no data records to display."
                            Font-Size="12pt" ForeColor="#333333" GridLines="None">
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" SortExpression="TotalCost" />
                            </Columns>
                            <RowStyle BackColor="#E3EAEB" />
                            <EditRowStyle BackColor="#7C6F57" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
                            DeleteCommand="DELETE FROM [Reservations] WHERE [ReserveNo] = @ReserveNo" InsertCommand="INSERT INTO [Reservations] ([Customer], [InventoryNo], [DateReserved], [Confirmed], [Paid], [Claimed], [OrderNo]) VALUES (@Customer, @InventoryNo, @DateReserved, @Confirmed, @Paid, @Claimed, @OrderNo)"
                            ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
                            SelectCommand="SELECT SUM(Vehicles.Price) AS TotalCost FROM Reservations INNER JOIN Vehicles ON Reservations.InventoryNo = Vehicles.InventoryNo"
                            UpdateCommand="UPDATE [Reservations] SET [Customer] = @Customer, [InventoryNo] = @InventoryNo, [DateReserved] = @DateReserved, [Confirmed] = @Confirmed, [Paid] = @Paid, [Claimed] = @Claimed, [OrderNo] = @OrderNo WHERE [ReserveNo] = @ReserveNo">
                            <DeleteParameters>
                                <asp:Parameter Name="ReserveNo" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Customer" Type="String" />
                                <asp:Parameter Name="InventoryNo" Type="Int32" />
                                <asp:Parameter Name="DateReserved" Type="DateTime" />
                                <asp:Parameter Name="Confirmed" Type="Boolean" />
                                <asp:Parameter Name="Paid" Type="Boolean" />
                                <asp:Parameter Name="Claimed" Type="Boolean" />
                                <asp:Parameter Name="OrderNo" Type="Int32" />
                                <asp:Parameter Name="ReserveNo" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Customer" Type="String" />
                                <asp:Parameter Name="InventoryNo" Type="Int32" />
                                <asp:Parameter Name="DateReserved" Type="DateTime" />
                                <asp:Parameter Name="Confirmed" Type="Boolean" />
                                <asp:Parameter Name="Paid" Type="Boolean" />
                                <asp:Parameter Name="Claimed" Type="Boolean" />
                                <asp:Parameter Name="OrderNo" Type="Int32" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </td>
                    <td style="width: 182px">
                        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            DataSourceID="SqlDataSource4" Font-Size="12pt" ForeColor="#333333" GridLines="None">
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:BoundField DataField="TotalItems" HeaderText="Total Items" ReadOnly="True" SortExpression="TotalItems" />
                            </Columns>
                            <RowStyle BackColor="#E3EAEB" />
                            <EditRowStyle BackColor="#7C6F57" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
                            DeleteCommand="DELETE FROM [Reservations] WHERE [ReserveNo] = @ReserveNo" InsertCommand="INSERT INTO [Reservations] ([Customer], [InventoryNo], [DateReserved], [Confirmed], [Paid], [Claimed], [OrderNo]) VALUES (@Customer, @InventoryNo, @DateReserved, @Confirmed, @Paid, @Claimed, @OrderNo)"
                            ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
                            SelectCommand="SELECT COUNT(*) AS TotalItems FROM Reservations " UpdateCommand="UPDATE [Reservations] SET [Customer] = @Customer, [InventoryNo] = @InventoryNo, [DateReserved] = @DateReserved, [Confirmed] = @Confirmed, [Paid] = @Paid, [Claimed] = @Claimed, [OrderNo] = @OrderNo WHERE [ReserveNo] = @ReserveNo">
                            <DeleteParameters>
                                <asp:Parameter Name="ReserveNo" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Customer" Type="String" />
                                <asp:Parameter Name="InventoryNo" Type="Int32" />
                                <asp:Parameter Name="DateReserved" Type="DateTime" />
                                <asp:Parameter Name="Confirmed" Type="Boolean" />
                                <asp:Parameter Name="Paid" Type="Boolean" />
                                <asp:Parameter Name="Claimed" Type="Boolean" />
                                <asp:Parameter Name="OrderNo" Type="Int32" />
                                <asp:Parameter Name="ReserveNo" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Customer" Type="String" />
                                <asp:Parameter Name="InventoryNo" Type="Int32" />
                                <asp:Parameter Name="DateReserved" Type="DateTime" />
                                <asp:Parameter Name="Confirmed" Type="Boolean" />
                                <asp:Parameter Name="Paid" Type="Boolean" />
                                <asp:Parameter Name="Claimed" Type="Boolean" />
                                <asp:Parameter Name="OrderNo" Type="Int32" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
            
            <span style="font-size: 9pt">&nbsp;<asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            DataSourceID="SqlDataSource5" Font-Size="12pt" ForeColor="#333333" GridLines="None" Width="100%">
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Address" HeaderText="Shipping Address" SortExpression="Address" />
                            </Columns>
                            <RowStyle BackColor="#E3EAEB" />
                            <EditRowStyle BackColor="#7C6F57" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        </span></span></center>
    <center>
                        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ExtraInfoConnectionString %>"
                            DeleteCommand="DELETE FROM [ExtraInfo] WHERE [CustomerNumber] = @CustomerNumber"
                            InsertCommand="INSERT INTO [ExtraInfo] ([UserID], [LicenseNumber], [TelephoneNumber], [Address], [DateCreated], [IPAddress], [FullName]) VALUES (@UserID, @LicenseNumber, @TelephoneNumber, @Address, @DateCreated, @IPAddress, @FullName)"
                            ProviderName="<%$ ConnectionStrings:ExtraInfoConnectionString.ProviderName %>"
                            SelectCommand="SELECT aspnet_Users.UserName, ExtraInfo.Address FROM aspnet_Users INNER JOIN ExtraInfo ON aspnet_Users.UserId = ExtraInfo.UserID"
                            UpdateCommand="UPDATE [ExtraInfo] SET [UserID] = @UserID, [LicenseNumber] = @LicenseNumber, [TelephoneNumber] = @TelephoneNumber, [Address] = @Address, [DateCreated] = @DateCreated, [IPAddress] = @IPAddress, [FullName] = @FullName WHERE [CustomerNumber] = @CustomerNumber">
                            <DeleteParameters>
                                <asp:Parameter Name="CustomerNumber" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="UserID" Type="Object" />
                                <asp:Parameter Name="LicenseNumber" Type="String" />
                                <asp:Parameter Name="TelephoneNumber" Type="String" />
                                <asp:Parameter Name="Address" Type="String" />
                                <asp:Parameter Name="DateCreated" Type="DateTime" />
                                <asp:Parameter Name="IPAddress" Type="String" />
                                <asp:Parameter Name="FullName" Type="String" />
                                <asp:Parameter Name="CustomerNumber" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:Parameter Name="UserID" Type="Object" />
                                <asp:Parameter Name="LicenseNumber" Type="String" />
                                <asp:Parameter Name="TelephoneNumber" Type="String" />
                                <asp:Parameter Name="Address" Type="String" />
                                <asp:Parameter Name="DateCreated" Type="DateTime" />
                                <asp:Parameter Name="IPAddress" Type="String" />
                                <asp:Parameter Name="FullName" Type="String" />
                            </InsertParameters>
                        </asp:SqlDataSource>
        &nbsp;</center>
    <center>
        &nbsp;</center>
    <center>
        <asp:Label ID="Label1" runat="server" Text="Method Of Payment:" Font-Bold="True" Font-Size="12pt"></asp:Label>&nbsp;</center>
    <center>
        <asp:RadioButton ID="RadioButton1" runat="server" GroupName="Methods" Text="Cash (Through Office)" Checked="True" />&nbsp;</center>
    <center>
        <asp:RadioButton ID="RadioButton2" runat="server" Text="Direct Deposit (Through Bank)" GroupName="Methods" />&nbsp;</center>
    <center>
        <asp:RadioButton ID="RadioButton3" runat="server" Text="Check" GroupName="Methods" />&nbsp;</center>
    <center>
        &nbsp;</center>
    <center>
        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="12pt" Text="Months To Pay:"></asp:Label>&nbsp;</center>
    <center>
        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:ListItem Selected="True">Full Payment</asp:ListItem>
            <asp:ListItem>3 Months</asp:ListItem>
            <asp:ListItem>6 Months</asp:ListItem>
            <asp:ListItem>12 Months</asp:ListItem>
        </asp:RadioButtonList>&nbsp;</center>
    <center>
        &nbsp;</center>
    <center>
        &nbsp;<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Confirm" /></center>
</asp:Content>

