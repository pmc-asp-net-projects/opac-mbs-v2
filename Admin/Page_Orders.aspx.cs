using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Page_Orders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        int Number;

        if (TextBox1.Text == "")
        {
            if (Request["OrderNo"] == null) TextBox1.Text = "1";
            else TextBox1.Text = Request["OrderNo"];
        }

        Number = int.Parse(TextBox1.Text); 
        
        SqlDataSource2.SelectCommand = "SELECT Reservations.ReserveNo, Reservations.OrderNo, Vehicles.InventoryNo, Vehicles.BodyType, Vehicles.ChassisNo, Vehicles.EngineNo, Vehicles.HorsePower, Vehicles.GasCapacity, Vehicles.ImagePath, Vehicles.Price FROM Reservations INNER JOIN Vehicles ON Reservations.InventoryNo = Vehicles.InventoryNo WHERE [OrderNo] = " + Number;
        SqlDataSource2.Select(DataSourceSelectArguments.Empty);
        
        SqlDataSource1.SelectCommand = "SELECT * FROM [Orders] WHERE [OrderNo] = " + Number;
        SqlDataSource1.Select(DataSourceSelectArguments.Empty);

        GridView1.DataBind();
    }
}
