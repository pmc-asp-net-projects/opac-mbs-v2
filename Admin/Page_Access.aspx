<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Access.aspx.cs" Inherits="Admin_Page_Access" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    <span style="font-size: 16pt"><strong>&nbsp;<br />
        Administrator Access&nbsp;<br />
        &nbsp;</strong></span><br />
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Page_Reservations.aspx">Cart / Orders</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="Page_Vehicles.aspx">Vehicle Details</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="Page_Members.aspx">Member Details</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="Page_Contractor.aspx">Contractor Details</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="Page_Logs.aspx">Logs</asp:HyperLink>
    </center>
    <center>
        &nbsp;</center>
    <center>
        <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="Page_NewEntry.aspx">Add Entry</asp:HyperLink>&nbsp;</center>
</asp:Content>

