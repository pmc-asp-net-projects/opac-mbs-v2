<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Logs.aspx.cs" Inherits="Page_Logs" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    <span style="font-size: 16pt">&nbsp;<br />
        <strong>Logs<br />
            &nbsp;</strong></span><br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="LogNumer" DataSourceID="SqlDataSource1"
        EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="Vertical" Width="90%">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="LogNumer" HeaderText="Log #" ReadOnly="True" SortExpression="LogNumer" />
            <asp:BoundField DataField="Source" HeaderText="Source" SortExpression="Source" />
            <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" />
            <asp:BoundField DataField="DateCreated" HeaderText="Date Created" SortExpression="DateCreated" />
            <asp:CommandField ShowEditButton="True" />
        </Columns>
        <RowStyle BackColor="#E3EAEB" />
        <EditRowStyle BackColor="#7C6F57" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [Logs] WHERE [LogNumer] = @LogNumer" InsertCommand="INSERT INTO [Logs] ([Source], [Message], [DateCreated]) VALUES (@Source, @Message, @DateCreated)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT [LogNumer], [Source], [Message], [DateCreated] FROM [Logs]"
        UpdateCommand="UPDATE [Logs] SET [Source] = @Source, [Message] = @Message, [DateCreated] = @DateCreated WHERE [LogNumer] = @LogNumer">
        <InsertParameters>
            <asp:Parameter Name="Source" Type="String" />
            <asp:Parameter Name="Message" Type="String" />
            <asp:Parameter Name="DateCreated" Type="DateTime" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Source" Type="String" />
            <asp:Parameter Name="Message" Type="String" />
            <asp:Parameter Name="DateCreated" Type="DateTime" />
            <asp:Parameter Name="LogNumer" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="LogNumer" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    </center>
</asp:Content>

