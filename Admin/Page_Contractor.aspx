<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Contractor.aspx.cs" Inherits="Admin_Page_Contractor" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    <span style="font-size: 16pt">&nbsp;<br />
        <strong>Contractor Details Page</strong></span><strong>&nbsp;<br />
        </strong><span style="font-size: 16pt">
            <br />
            <span style="font-size: 12pt">Please enter the Contractor Number:
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Search" /></span><br />
            &nbsp;<br />
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" CellPadding="4"
                DataKeyNames="ContractorNumber" DataSourceID="SqlDataSource2" Font-Size="12pt"
                ForeColor="#333333" GridLines="None" Height="50px" Width="25%">
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                <EditRowStyle BackColor="#7C6F57" />
                <RowStyle BackColor="#E3EAEB" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <Fields>
                    <asp:BoundField DataField="ContractorNumber" HeaderText="Contractor Number" InsertVisible="False"
                        ReadOnly="True" SortExpression="ContractorNumber" />
                    <asp:BoundField DataField="ContractorName" HeaderText="Contractor Name" SortExpression="ContractorName" />
                </Fields>
                <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:DetailsView>
            <br />
            <strong>Vehicles Made:</strong><br />
            &nbsp;<br />
        </span>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1"
        EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="Vertical" Width="90%">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="InventoryNo" HeaderText="Inventory #" InsertVisible="False"
                ReadOnly="True" SortExpression="InventoryNo" />
            <asp:ImageField DataImageUrlField="ImagePath">
            </asp:ImageField>
            <asp:BoundField DataField="ChassisNo" HeaderText="Chassis #" SortExpression="ChassisNo" />
            <asp:BoundField DataField="BodyType" HeaderText="Body Type" SortExpression="BodyType" />
            <asp:BoundField DataField="EngineNo" HeaderText="Engine #" SortExpression="EngineNo" />
            <asp:BoundField DataField="HorsePower" HeaderText="Horse Power" SortExpression="HorsePower" />
            <asp:BoundField DataField="GasCapacity" HeaderText="Gas Capacity" SortExpression="GasCapacity" />
            <asp:BoundField DataField="Features" HeaderText="Features" SortExpression="Features" />
            <asp:BoundField DataField="ContractorNumber" HeaderText="Contractor #" SortExpression="ContractorNumber" />
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
        <RowStyle BackColor="#E3EAEB" />
        <EditRowStyle BackColor="#7C6F57" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [Vehicles] WHERE [InventoryNo] = @InventoryNo" InsertCommand="INSERT INTO [Vehicles] ([ChassisNo], [BodyType], [EngineNo], [HorsePower], [GasCapacity], [Features], [ImagePath], [ContractorNumber], [Price]) VALUES (@ChassisNo, @BodyType, @EngineNo, @HorsePower, @GasCapacity, @Features, @ImagePath, @ContractorNumber, @Price)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT * FROM [Vehicles]"
        UpdateCommand="UPDATE [Vehicles] SET [ChassisNo] = @ChassisNo, [BodyType] = @BodyType, [EngineNo] = @EngineNo, [HorsePower] = @HorsePower, [GasCapacity] = @GasCapacity, [Features] = @Features, [ImagePath] = @ImagePath, [ContractorNumber] = @ContractorNumber, [Price] = @Price WHERE [InventoryNo] = @InventoryNo">
        <InsertParameters>
            <asp:Parameter Name="ChassisNo" Type="String" />
            <asp:Parameter Name="BodyType" Type="String" />
            <asp:Parameter Name="EngineNo" Type="String" />
            <asp:Parameter Name="HorsePower" Type="Int32" />
            <asp:Parameter Name="GasCapacity" Type="Int32" />
            <asp:Parameter Name="Features" Type="String" />
            <asp:Parameter Name="ImagePath" Type="String" />
            <asp:Parameter Name="ContractorNumber" Type="Int32" />
            <asp:Parameter Name="Price" Type="Decimal" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ChassisNo" Type="String" />
            <asp:Parameter Name="BodyType" Type="String" />
            <asp:Parameter Name="EngineNo" Type="String" />
            <asp:Parameter Name="HorsePower" Type="Int32" />
            <asp:Parameter Name="GasCapacity" Type="Int32" />
            <asp:Parameter Name="Features" Type="String" />
            <asp:Parameter Name="ImagePath" Type="String" />
            <asp:Parameter Name="ContractorNumber" Type="Int32" />
            <asp:Parameter Name="Price" Type="Decimal" />
            <asp:Parameter Name="InventoryNo" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="InventoryNo" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        SelectCommand="SELECT * FROM [ContractorTable]"></asp:SqlDataSource>
        </center>
</asp:Content>

