<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_LoggedIn.aspx.cs" Inherits="Page_LoggedIn" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
    <span style="font-size: 16pt"><span style="font-size: 12pt"></span><br />
        <span style="font-size: 9pt">&nbsp;</span></span>
        &nbsp;<asp:Label ID="ErrorMessageLabel" runat="server" Font-Size="16pt" ForeColor="Red"
            Text="You Can Add Upto 3 Items Only" Visible="False"></asp:Label></center>
    <center>
        <span style="font-size: 16pt"><span style="font-size: 9pt">&nbsp;</span><br />
        <strong>Cataloge</strong><br />
            &nbsp;</span></center>
    <center>
        <span style="font-size: 16pt">
            <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="SqlDataSource4" Font-Size="12pt" ForeColor="#333333" GridLines="None">
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="TotalItems" HeaderText="Total Reserved Items" SortExpression="TotalItems" />
                </Columns>
                <RowStyle BackColor="#E3EAEB" />
                <EditRowStyle BackColor="#7C6F57" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
                DeleteCommand="DELETE FROM [Reservations] WHERE [ReserveNo] = @ReserveNo" InsertCommand="INSERT INTO [Reservations] ([Customer], [InventoryNo], [DateReserved], [Confirmed], [Paid], [Claimed], [OrderNo]) VALUES (@Customer, @InventoryNo, @DateReserved, @Confirmed, @Paid, @Claimed, @OrderNo)"
                ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
                SelectCommand="SELECT COUNT(*) AS TotalItems FROM Reservations " UpdateCommand="UPDATE [Reservations] SET [Customer] = @Customer, [InventoryNo] = @InventoryNo, [DateReserved] = @DateReserved, [Confirmed] = @Confirmed, [Paid] = @Paid, [Claimed] = @Claimed, [OrderNo] = @OrderNo WHERE [ReserveNo] = @ReserveNo">
                <DeleteParameters>
                    <asp:Parameter Name="ReserveNo" Type="Int32" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Customer" Type="String" />
                    <asp:Parameter Name="InventoryNo" Type="Int32" />
                    <asp:Parameter Name="DateReserved" Type="DateTime" />
                    <asp:Parameter Name="Confirmed" Type="Boolean" />
                    <asp:Parameter Name="Paid" Type="Boolean" />
                    <asp:Parameter Name="Claimed" Type="Boolean" />
                    <asp:Parameter Name="OrderNo" Type="Int32" />
                    <asp:Parameter Name="ReserveNo" Type="Int32" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:Parameter Name="Customer" Type="String" />
                    <asp:Parameter Name="InventoryNo" Type="Int32" />
                    <asp:Parameter Name="DateReserved" Type="DateTime" />
                    <asp:Parameter Name="Confirmed" Type="Boolean" />
                    <asp:Parameter Name="Paid" Type="Boolean" />
                    <asp:Parameter Name="Claimed" Type="Boolean" />
                    <asp:Parameter Name="OrderNo" Type="Int32" />
                </InsertParameters>
            </asp:SqlDataSource>
        </span>
    </center>
    <center>
        &nbsp;<span style="font-size: 16pt">
            <table style="width: 355px" border="1">
                <tr>
                    <td colspan="2" style="text-align: center">
                        <span style="font-size: 12pt"><strong>
                        SEARCH OPTIONS</strong></span></td>
                </tr>
                <tr>
                    <td>
                        <span style="font-size: 9pt">Search For ChassisNo:</span></td>
                    <td>
                        <asp:TextBox ID="ChassisNoTextBox" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span style="font-size: 9pt">
                        Search For Body Type:</span></td>
                    <td>
    <asp:TextBox ID="BodyTypeTextBox" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span style="font-size: 9pt">Search For EngineNo:</span></td>
                    <td>
                        <asp:TextBox ID="EngineNoTextBox" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span style="font-size: 9pt">Search For HorsePower:</span></td>
                    <td>
                        <asp:TextBox ID="HorsePowerTextBox" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span style="font-size: 9pt">Search For GasCapacity:</span></td>
                    <td>
                        <asp:TextBox ID="GasCapacityTextBox" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <span style="font-size: 9pt">Search For ContractorName:</span></td>
                    <td>
                        <asp:TextBox ID="ContractorNameTextBox" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td align="center">
                        <asp:Button ID="Button1"
        runat="server" Text="Search" OnClick="Button1_Click" /></td>
                </tr>
            </table>
            <br />
    <asp:Label ID="Label1" runat="server" Text="Label" Font-Size="12pt"></asp:Label><br />
    <br />
    <asp:Button ID="PreviousButton" runat="server" OnClick="PreviousButton_Click" Text="Previous" />
    <asp:Button ID="NextButton" runat="server" OnClick="NextButton_Click" Text="Next" /><br />
        </span>
    <br />
    
    <%-- <asp:DataList ID="DataList1" runat="server" DataKeyField="InventoryNo" DataSourceID="SqlDataSource1"> --%>
    
    <asp:DataList ID="DataList1" runat="server" DataKeyField="InventoryNo" OnItemCommand="DataList1_ItemCommand" Width="50%" OnSelectedIndexChanged="DataList1_SelectedIndexChanged">
        <ItemTemplate>
            <table width="100%">
                <tr>
                    <td align="center" valign="middle">
                        <a href='<%# "Page_Vehicles.aspx?VehicleNumber=" + Eval("InventoryNo") %>'><asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("ImagePath") %>' /></a></td>
                    <td rowspan="2">
            InventoryNo:
            <asp:HyperLink ID="InventoryNoLink" runat="server" Text='<%# Eval("InventoryNo") %>' NavigateUrl='<%# "Page_Vehicles.aspx?VehicleNumber=" + Eval("InventoryNo") %>'>
            </asp:HyperLink><br />
            ChassisNo:
            <asp:Label ID="ChassisNoLabel" runat="server" Text='<%# Eval("ChassisNo") %>'></asp:Label><br />
            BodyType:
            <asp:Label ID="BodyTypeLabel" runat="server" Text='<%# Eval("BodyType") %>'></asp:Label><br />
            EngineNo:
            <asp:Label ID="EngineNoLabel" runat="server" Text='<%# Eval("EngineNo") %>'></asp:Label><br />
            HorsePower:
            <asp:Label ID="HorsePowerLabel" runat="server" Text='<%# Eval("HorsePower") %>'>
            </asp:Label><br />
            GasCapacity:
            <asp:Label ID="GasCapacityLabel" runat="server" Text='<%# Eval("GasCapacity") %>'>
            </asp:Label><br />
            Price:
            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Price") %>'>
            </asp:Label><br />            
            ContractorName:
            <asp:HyperLink ID="ContractorName" runat="server" Text='<%# Eval("ContractorName") %>' NavigateUrl='<%# "Page_Contractor.aspx?ContractorNumber=" + Eval("ContractorNumber") %>'>
            </asp:HyperLink></td>
                </tr>
                <tr>
                    <td align="center" valign="middle">
                        </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" bgcolor="#339966" colspan="2">
                    <table style="width: 100%">
                            <tr>
                                <td>
                        <asp:Button ID="ReserveButton" runat="server" Text="Add To Cart" /></td>
                                <td align="right">
                                    <asp:Button ID="MoreOptionButton" runat="server" Text="More Info / Options" OnClick="Button2_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />

        </ItemTemplate>
    </asp:DataList><br />
    <br />
        <span style="font-size: 8pt">Note: The picture may vary from the original.</span><br />
    <br />
            <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="InventoryNo" DataSourceID="SqlDataSource1"
        EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="None"
        OnSelectedIndexChanged="GridView1_SelectedIndexChanged" Visible="False">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="InventoryNo" HeaderText="InventoryNo" ReadOnly="True"
                SortExpression="InventoryNo" />
            <asp:ImageField DataImageUrlField="ImagePath">
            </asp:ImageField>
            <asp:BoundField DataField="ChassisNo" HeaderText="ChassisNo" SortExpression="ChassisNo" />
            <asp:BoundField DataField="BodyType" HeaderText="BodyType" SortExpression="BodyType" />
            <asp:BoundField DataField="EngineNo" HeaderText="EngineNo" SortExpression="EngineNo" />
            <asp:BoundField DataField="HorsePower" HeaderText="HorsePower" SortExpression="HorsePower" />
            <asp:BoundField DataField="GasCapacity" HeaderText="GasCapacity" SortExpression="GasCapacity" />
            <asp:BoundField DataField="Features" HeaderText="Features" SortExpression="Features" />
            <asp:HyperLinkField DataNavigateUrlFields="ContractorNumber" DataNavigateUrlFormatString="Page_Contractor.aspx?ContractorNumber={0}"
                DataTextField="ContractorName" HeaderText="Contractor Name" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
        </Columns>
        <RowStyle BackColor="#E3EAEB" />
        <EditRowStyle BackColor="#7C6F57" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [Vehicles] WHERE [InventoryNo] = @InventoryNo" InsertCommand="INSERT INTO Vehicles(ChassisNo, BodyType, EngineNo, HorsePower, GasCapacity, Features, ImagePath, ContractorNumber) VALUES (@ChassisNo, @BodyType, @EngineNo, @HorsePower, @GasCapacity, @Features, @ImagePath, @ContractorNumber)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT Vehicles.InventoryNo, Vehicles.ChassisNo, Vehicles.BodyType, Vehicles.EngineNo, Vehicles.HorsePower, Vehicles.GasCapacity, Vehicles.Features, Vehicles.ImagePath, Vehicles.Price, ContractorTable.ContractorName, ContractorTable.ContractorNumber FROM ContractorTable INNER JOIN Vehicles ON ContractorTable.ContractorNumber = Vehicles.ContractorNumber"
        UpdateCommand="UPDATE Vehicles SET ChassisNo = @ChassisNo, BodyType = @BodyType, EngineNo = @EngineNo, HorsePower = @HorsePower, GasCapacity = @GasCapacity, Features = @Features, ImagePath = @ImagePath, ContractorNumber = @ContractorNumber WHERE (InventoryNo = @InventoryNo)">
        <InsertParameters>
            <asp:Parameter Name="ChassisNo" Type="String" />
            <asp:Parameter Name="BodyType" Type="String" />
            <asp:Parameter Name="EngineNo" Type="String" />
            <asp:Parameter Name="HorsePower" Type="Int32" />
            <asp:Parameter Name="GasCapacity" Type="Int32" />
            <asp:Parameter Name="Features" Type="String" />
            <asp:Parameter Name="ImagePath" Type="String" />
            <asp:Parameter Name="ContractorNumber" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ChassisNo" Type="String" />
            <asp:Parameter Name="BodyType" Type="String" />
            <asp:Parameter Name="EngineNo" Type="String" />
            <asp:Parameter Name="HorsePower" Type="Int32" />
            <asp:Parameter Name="GasCapacity" Type="Int32" />
            <asp:Parameter Name="Features" Type="String" />
            <asp:Parameter Name="ImagePath" Type="String" />
            <asp:Parameter Name="ContractorNumber" />
            <asp:Parameter Name="InventoryNo" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="InventoryNo" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    &nbsp;&nbsp;<br />
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [Reservations] WHERE [ReserveNo] = @ReserveNo" InsertCommand="INSERT INTO [Reservations] ([Customer], [InventoryNo], [DateReserved]) VALUES (@Customer, @InventoryNo, @DateReserved)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT [ReserveNo], [Customer], [InventoryNo], [DateReserved] FROM [Reservations]"
        UpdateCommand="UPDATE [Reservations] SET [Customer] = @Customer, [InventoryNo] = @InventoryNo, [DateReserved] = @DateReserved WHERE [ReserveNo] = @ReserveNo">
        <InsertParameters>
            <asp:Parameter Name="Customer" Type="String" />
            <asp:Parameter Name="InventoryNo" Type="Int32" />
            <asp:Parameter Name="DateReserved" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Customer" Type="String" />
            <asp:Parameter Name="InventoryNo" Type="Int32" />
            <asp:Parameter Name="DateReserved" Type="String" />
            <asp:Parameter Name="ReserveNo" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="ReserveNo" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    </center>
</asp:Content>

