<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
        <img src="Images/GLC%20Animated.gif" />&nbsp;</center>
    <br />
    <center><span style="font-size: 14pt"><strong>GLC Truck and Equipment Website</strong></span></center><br />
    <br />
    <span style="font-size: 10pt; font-family: Tahoma"></span>
    <div style="text-align: justify">
        <font color="#000000" face="Arial" style="font-size: 9pt">GLC Trucks &amp; Equipment
            offers a wide array and variety of transport choices that meet varied requirements.
            The Company provides vans, dump trucks, fire trucks, and other vehicles and products.
            We have our very own spacious plant in Valenzuela where we can customize trucks
            and automobiles to suit your needs and requirements. We ensure that all our products
            pass the strictest and most stringent standards of quality, safety and durability.
        </font>
    </div>
    <div>
        <font color="#000000" face="Arial" style="font-size: 9pt"></font>&nbsp;</div>
    <div>
        <font color="#000000" face="Arial" style="font-size: 9pt">
            <br />
            We also have the facility to fabricate body work for special customized trucks.&nbsp;
            Having our own plant likewise Allows us to provide our clients superior after-sales
            service.<br />
            <br />
            <center><span style="font-size: 14pt"><strong>Our Locations</strong></span><br />
            <br />
            <img src="Images/GLC%20Front.gif" /><br />
            <br />
            <span style="font-size: 14pt"><strong>GLC Main Branch</strong></span><br />
            <span style="font-size: 12pt"><em>#933 Edsa Philam, Quezon City</em></span><br />
            Service Telephone Number: 410-92-24, or 410-21-55<br />
            <br />
            <br />
            <img src="Images/DSC02120b.JPG" /><br />
            <br />
            <span style="font-size: 14pt"><strong>GLC Bonifacio Branch</strong></span><br />
            <span style="font-size: 12pt"><em>#518-C A. Bonifacio Street, Quezon City</em></span><br />
            Service Telephone Number: 361-41-50, or 362-47-87, or 366-45-80<br />
            <br />
            <img src="Images/Service.gif" /><br />
            <br />
            <span style="font-size: 14pt"><strong>GLC Plant</strong></span><br />
            <span style="font-size: 12pt"><em>#6070 Pabaya Street., Mapulang Lupa, Valenzuela City</em></span><br />
            Service Telephone Number: 984-25-09, or 445-32-26</center></font></div>
</asp:Content>

