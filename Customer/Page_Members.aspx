<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Members.aspx.cs" Inherits="Page_Members" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    <span style="font-size: 16pt">&nbsp;<br />
        <strong>Members<br />
            <br />
        </strong><span style="font-size: 12pt">Username:
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>&nbsp;<asp:Button ID="Button1"
                runat="server" Text="Search" OnClick="Button1_Click" /></span><br />
        <strong>&nbsp;</strong><asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True"
            AutoGenerateRows="False" CellPadding="4" DataSourceID="SqlDataSource1" Font-Bold="False"
            Font-Size="9pt" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <EditRowStyle BackColor="#7C6F57" />
            <RowStyle BackColor="#E3EAEB" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <Fields>
                <asp:BoundField DataField="FullName" HeaderText="Full Name" SortExpression="FullName" />
                <asp:BoundField DataField="UserName" HeaderText="User Name" SortExpression="UserName" />
                <asp:BoundField DataField="CustomerNumber" HeaderText="Customer #" InsertVisible="False"
                    ReadOnly="True" SortExpression="CustomerNumber" />
                <asp:BoundField DataField="LicenseNumber" HeaderText="License #" SortExpression="LicenseNumber" />
                <asp:BoundField DataField="TelephoneNumber" HeaderText="Telephone #" SortExpression="TelephoneNumber" />
                <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                <asp:BoundField DataField="DateCreated" HeaderText="Date Created" SortExpression="DateCreated" />
                <asp:BoundField DataField="IPAddress" HeaderText="IP Address" SortExpression="IPAddress" />
            </Fields>
            <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
        <strong></strong>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExtraInfoConnectionString %>"
            DeleteCommand="DELETE FROM [ExtraInfo] WHERE [CustomerNumber] = @CustomerNumber"
            InsertCommand="INSERT INTO [ExtraInfo] ([UserID], [LicenseNumber], [TelephoneNumber], [Address], [DateCreated], [IPAddress]) VALUES (@UserID, @LicenseNumber, @TelephoneNumber, @Address, @DateCreated, @IPAddress)"
            ProviderName="<%$ ConnectionStrings:ExtraInfoConnectionString.ProviderName %>"
            SelectCommand="SELECT aspnet_Users.UserName, ExtraInfo.CustomerNumber, ExtraInfo.LicenseNumber, ExtraInfo.TelephoneNumber, ExtraInfo.Address, ExtraInfo.DateCreated, ExtraInfo.IPAddress, ExtraInfo.FullName FROM aspnet_Users INNER JOIN ExtraInfo ON aspnet_Users.UserId = ExtraInfo.UserID"
            UpdateCommand="UPDATE [ExtraInfo] SET [UserID] = @UserID, [LicenseNumber] = @LicenseNumber, [TelephoneNumber] = @TelephoneNumber, [Address] = @Address, [DateCreated] = @DateCreated, [IPAddress] = @IPAddress WHERE [CustomerNumber] = @CustomerNumber">
            <DeleteParameters>
                <asp:Parameter Name="CustomerNumber" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="UserID" Type="Object" />
                <asp:Parameter Name="LicenseNumber" Type="String" />
                <asp:Parameter Name="TelephoneNumber" Type="String" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="DateCreated" Type="DateTime" />
                <asp:Parameter Name="IPAddress" Type="String" />
                <asp:Parameter Name="CustomerNumber" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="UserID" Type="Object" />
                <asp:Parameter Name="LicenseNumber" Type="String" />
                <asp:Parameter Name="TelephoneNumber" Type="String" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="DateCreated" Type="DateTime" />
                <asp:Parameter Name="IPAddress" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>
        <strong></strong></span>
        </center>
</asp:Content>

