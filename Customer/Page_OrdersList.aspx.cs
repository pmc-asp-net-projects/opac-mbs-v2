using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_Page_OrdersList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue == "All")
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM [Orders] WHERE [Customer] = '" + User.Identity.Name.ToString() + "'";
        }
        else if (DropDownList1.SelectedValue == "Finished")
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM [Orders] WHERE [Status] = 'FINISHED' AND [Customer] = '" + User.Identity.Name.ToString() + "'";
        }
        else if (DropDownList1.SelectedValue == "Pending")
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM [Orders] WHERE [Status] <> 'FINISHED' AND [Customer] = '" + User.Identity.Name.ToString() + "'";
        }

        SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        GridView1.DataBind();

    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
