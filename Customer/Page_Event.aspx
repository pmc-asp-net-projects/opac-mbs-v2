<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Event.aspx.cs" Inherits="Page_Event" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    <span style="font-size: 16pt"><strong>
    &nbsp;<br />
    Events And News<br />
    &nbsp;</strong></span><br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="EventNo" DataSourceID="SqlDataSource1"
        EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="Vertical" Width="90%">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="PostDate" HeaderText="Date Time Posted" SortExpression="PostDate" >
                <ItemStyle Width="1px" />
            </asp:BoundField>
            <asp:BoundField DataField="Sender" HeaderText="Sender" SortExpression="Sender" >
                <ItemStyle Width="1px" />
            </asp:BoundField>
            <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" >
                <ItemStyle HorizontalAlign="Left" Width="100%" />
            </asp:BoundField>
        </Columns>
        <RowStyle BackColor="#E3EAEB" />
        <EditRowStyle BackColor="#7C6F57" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
        DeleteCommand="DELETE FROM [EventTable] WHERE [EventNo] = @EventNo" InsertCommand="INSERT INTO [EventTable] ([PostDate], [Sender], [Message]) VALUES (@PostDate, @Sender, @Message)"
        ProviderName="<%$ ConnectionStrings:DatabaseConnectionString1.ProviderName %>"
        SelectCommand="SELECT [EventNo], [PostDate], [Sender], [Message] FROM [EventTable]"
        UpdateCommand="UPDATE [EventTable] SET [PostDate] = @PostDate, [Sender] = @Sender, [Message] = @Message WHERE [EventNo] = @EventNo">
        <InsertParameters>
            <asp:Parameter Name="PostDate" Type="DateTime" />
            <asp:Parameter Name="Sender" Type="String" />
            <asp:Parameter Name="Message" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="PostDate" Type="DateTime" />
            <asp:Parameter Name="Sender" Type="String" />
            <asp:Parameter Name="Message" Type="String" />
            <asp:Parameter Name="EventNo" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="EventNo" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    </center>
    <center>
        &nbsp;</center>
</asp:Content>

