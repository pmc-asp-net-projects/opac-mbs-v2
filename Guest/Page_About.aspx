<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_About.aspx.cs" Inherits="Page_About" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
        <img src="../Images/Event1.gif" /><br />
        &nbsp;</center>
    <br />
    <span style="font-size: 10pt; font-family: Tahoma"></span>
    <div style="text-align: justify">
        <font color="#000000" face="Arial" style="font-size: 9pt">GLC Truck &amp; Equipment
            was established in 1999 by Ms. Ruby Navarro, a former banker, who saw the need to
            provide a superior yet affordable alternative to entrepreneurs and business organizations
            in need of quality trucks and equipment. The Company was envisioned as a professional
            business enterprise that would tap into the inherent creativity and craftsmanship
            of Filipino workers in providing trucks and equipment that were of superior quality
            and workmanship, at the same time foster professionalism and the growth of a viable
            industry.</font></div>
    <div>
        <font color="#000000" face="Arial" style="font-size: 9pt"></font>&nbsp;</div>
    <div>
        <font color="#000000" face="Arial" style="font-size: 9pt">
            <br />
            From its inception, GLC Truck &amp; Equipment has distinguished itself as a professionally-managed
            company. GLC is a pioneer of many innovations, including after-sales service, total
            customer care, and focus on quality and safety. In less than five years, GLC Truck
            &amp; Equipment has built a sterling and solid reputation as provider of the highest
            quality in terms of both products and service.</font></div>
    <div>
        <font color="#000000" face="Arial" style="font-size: 9pt"></font>&nbsp;</div>
    <div>
        <font color="#000000" face="Arial" style="font-size: 9pt">
            <br />
            Today, GLC Truck &amp; Equipment is leading the way towards further professionalizing
            the industry and showcasing the Ingenuity and talent of Filipino workers.</font></div>
</asp:Content>

