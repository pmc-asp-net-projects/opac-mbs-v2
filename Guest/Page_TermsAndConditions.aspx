<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_TermsAndConditions.aspx.cs" Inherits="Page_TermsAndConditions" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><span style="font-size: 16pt">&nbsp;<br />
        <strong>Terms And Conditions</strong><br />
        &nbsp;</span></center><br />
    <span style="font-size: 12pt; font-family: Times New Roman"><strong>TERMS OF USE </strong>
        <br />
        <br />
        Welcome to GLCTruckNEquip.com, a website owned and operated by GLCTruckNEquip.com.
        These Terms of Use govern your use of the GLCTruckNEquip.com website. By visiting
        or shopping at the GLCTruckNEquip.com website, you agree to be bound by these Terms
        of Use, as well as the other policies posted on the GLCTruckNEquip.com website.<br />
        <br />
        <strong>Prohibited Uses of the Site</strong><br />
        <br />
        The licenses set forth in these Terms of Use do not include any rights to:&nbsp;<br />
        <br />
    </span>
    <ul>
        <li><span style="font-size: 12pt; font-family: Times New Roman">interfere or attempt
            to interfere with the proper operation of the Site;&nbsp;</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">bypass any measures
            used to prevent or restrict access to any portion of the Site;</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">use any robot, spider,
            data-miner, scraper or other automated means to access the Site or its systems for
            any purpose or to copy, probe, test or monitor any portion of the Site, or in any
            way reproduce or circumvent the navigational structure or presentation of the Site,
            or to obtain or attempt to obtain any materials, documents or information through
            any means not purposely made available through the Site;</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">take any action that
            imposes an unreasonable or disproportionately large load on the infrastructure of
            the Site or any of the systems or networks comprising or connected to the Site;
            * compile, repackage, disseminate or otherwise use data available on or extracted
            from the Site, including product information and prices;</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">reproduce, record, retransmit,
            sell, rent, distribute, publish, post, or perform any of the content of the Site;</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">modify, download (other
            than page caching), reproduce, copy, or resell the Site, the content or any portion
            or derivative thereof;</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">copy or download any
            the account information of any user of the Site for the benefit of any other person
            or entity;</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">frame or utilize framing
            techniques to enclose any trademark, logo, or other proprietary information (including
            images, text, page layout, or form) of GLCTruckNEquip.com</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">use GLCTruckNEquip.com�s
            name, trademarks, service marks or logos in any meta tags or any other �hidden text�;
            or;</span></li><li><span style="font-size: 12pt; font-family: Times New Roman">use GLCTruckNEquip.com�s
            logo or other proprietary graphic or trademark as part of a link without express
            written permission.</span></li></ul>
    <span style="font-size: 12pt; font-family: Times New Roman">Each of the foregoing is
        expressly prohibited unless GLCTruckNEquip.com�s prior express written consent has
        been given. Any unauthorized use of the Site or any portion or derivative thereof
        shall terminate any license or permission granted by GLCTruckNEquip.com.
        <br />
        <br />
        <strong>Termination of Access</strong><br />
        <br />
        You agree that GLCTruckNEquip.com may, in its sole discretion and without prior
        notice to you, terminate or suspend your access to all or any part of the Site and
        your account without notice, for any reason, including without limitation: (1) attempts
        to gain unauthorized access to the Site or assistance to others� attempting to do
        so, (2) attempting to overcome software security features limiting use of or protecting
        any content available on the Site, (3) discontinuance or material modification of
        the Site or any service available on or through the Site, (4) violations of these
        Terms of Use or applicable law, (5) failure to pay for purchases, (6) suspected
        or actual copyright or trademark infringement, (7) unexpected operational difficulties,
        or (8) requests by law enforcement or other government agencies. YOU AGREE THAT
        GLCTruckNEquip.com WILL NOT BE LIABLE TO YOU OR TO ANY THIRD PARTY FOR TERMINATION
        OF YOUR ACCESS TO THE SITE.
        <br />
        <br />
        <strong>Copyright</strong><br />
        <br />
        All content included on the Site, including text, graphics, logos, user interfaces,
        button icons, images, audio clips, digital downloads, data compilations, and software,
        is the property of GLCTruckNEquip.com or its suppliers, licensors or content suppliers
        and is protected by applicable copyright laws. The design, selection, arrangement,
        coordination and compilation of all content on the Site are the exclusive property
        of GLCTruckNEquip.com and protected by applicable copyright laws. All software used
        on the Site is the property of GLCTruckNEquip.com or its software suppliers and
        protected by applicable copyright laws. </span>
</asp:Content>

