<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_GuestAccess2.aspx.cs" Inherits="Guest_Page_GuestAccess" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    &nbsp;<span style="font-size: 16pt">&nbsp;</span><br />
    <span style="font-size: 16pt"><strong>Guest Access</strong></span><br />
    <span style="font-size: 16pt">
    &nbsp;<br />
        <asp:HyperLink ID="HyperLink10" runat="server" Font-Size="9pt" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></span></center>
    <center>
        &nbsp;</center>
    <center>
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Guest/Page_Event.aspx">Events And News</asp:HyperLink><br />
    <br />
    <span style="font-size: 12pt"><strong>About Us</strong></span><br />
    <br />
    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Guest/Page_Values.aspx">Mission And Vision</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Guest/Page_About.aspx">History</asp:HyperLink>
    <br />
    <br />
    <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Guest/Page_TermsAndConditions.aspx">Terms And Condition</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Guest/Page_Service.aspx">Services</asp:HyperLink><br />
    <br />
    <strong><span style="font-size: 12pt">Vehicles</span></strong><br />
    <br />
    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="Page_LoggedIn.aspx">Catalogue</asp:HyperLink><br />
    <br />
    <strong><span style="font-size: 12pt">Photos</span></strong><br />
    <br />
    <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/Guest/Vehicles/Page_Passenger.aspx">Passenger Van</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/Guest/Vehicles/Page_SUV.aspx">Service Utility Vehicle</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/Guest/Vehicles/Page_Truck.aspx">Truck</asp:HyperLink><br />
    </center>
</asp:Content>

