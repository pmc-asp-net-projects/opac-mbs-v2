using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Page_LoggedIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ItemsGet();
        }
    }
    
    protected void Page_PreRender(object sender, EventArgs e)
    {
        UpdatePage();
    }

    private void UpdatePage()
    {
        SqlDataSource1.SelectCommand = "SELECT Vehicles.InventoryNo, Vehicles.ChassisNo, Vehicles.BodyType, Vehicles.EngineNo, Vehicles.HorsePower, Vehicles.GasCapacity, Vehicles.Features, Vehicles.ImagePath, Vehicles.Price, ContractorTable.ContractorName, ContractorTable.ContractorNumber FROM ContractorTable INNER JOIN Vehicles ON ContractorTable.ContractorNumber = Vehicles.ContractorNumber WHERE " +
            " [ChassisNo] LIKE '%" + ChassisNoTextBox.Text + "%'" +
            " AND [BodyType] LIKE '%" + BodyTypeTextBox.Text + "%'" +
            " AND [EngineNo] LIKE '%" + EngineNoTextBox.Text + "%'" +
            " AND [HorsePower] LIKE '%" + HorsePowerTextBox.Text + "%'" +
            " AND [GasCapacity] LIKE '%" + GasCapacityTextBox.Text + "%'" +
            " AND [ContractorName] LIKE '%" + ContractorNameTextBox.Text + "%'";
        SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        ItemsGet();

        SqlDataSource4.SelectCommand = "SELECT COUNT(*) AS TotalItems FROM Reservations  WHERE [OrderNo] IS NULL AND [Customer] = '" + User.Identity.Name.ToString() + "'";
        SqlDataSource4.Select(DataSourceSelectArguments.Empty);

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        GridView1.PageIndex = 0;
        CurrentIndex = 0;
        UpdatePage();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlDataSource2.InsertCommand = "INSERT INTO [Reservations] ([Customer], [InventoryNo], [DateReserved]) VALUES ('" + User.Identity.Name.ToString() + "', " + GridView1.SelectedRow.Cells[0].Text + ", '" + DateTime.Today.ToShortDateString() + "')";
        SqlDataSource2.Insert();
    }

    private void ItemsGet()
    {
        // SPECIAL THANKS TO: Limno, http://forums.asp.net/p/1070947/1562090.aspx#1562090
        DataView dv = (DataView)(SqlDataSource1.Select(DataSourceSelectArguments.Empty));
        DataSet newDS = new DataSet();
        DataTable dt = dv.Table.Clone();
        foreach (DataRowView drv in dv)
            dt.ImportRow(drv.Row);
        newDS.Tables.Add(dt);

        // SPECIAL THANKS TO: Various
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = newDS.Tables[0].DefaultView;
        objPds.AllowPaging = true;
        objPds.PageSize = 5;
        objPds.CurrentPageIndex = CurrentIndex;

        Label1.Text = "Page: " + (CurrentIndex + 1).ToString() + " of " + objPds.PageCount.ToString();

        PreviousButton.Enabled = !objPds.IsFirstPage;
        NextButton.Enabled = !objPds.IsLastPage;

        DataList1.DataSource = objPds;
        DataList1.DataBind();
    }
    public int CurrentIndex
    {
        // SPECIAL THANKS TO: Various
        get
        {
            if (ViewState["Index"] == null)
                return 0;
            else
                return (int)ViewState["Index"];
        }

        set
        {
            this.ViewState["Index"] = value;
        }
    }
    protected void PreviousButton_Click(object sender, EventArgs e)
    {
        CurrentIndex -= 1;
        ItemsGet();
    }
    protected void NextButton_Click(object sender, EventArgs e)
    {
        CurrentIndex += 1;
        ItemsGet();
    }

    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        // SPECIAL THANKS: http://msdn2.microsoft.com/En-US/library/98fy3w7e(VS.71).aspx

        // SPECIAL THANKS: Microsoft, http://msdn2.microsoft.com/en-us/library/system.web.ui.webcontrols.datalistcommandeventargs.commandsource.aspx
        if (((Button)e.CommandSource).Text == "Add To Cart")
        {
            if (GridView3.Rows[0].Cells[0].Text != "3")
            {
                // SPECIAL THANKS: http://forums.asp.net/p/1096597/1657017.aspx#1657017
                SqlDataSource2.InsertCommand = "INSERT INTO [Reservations] ([Customer], [InventoryNo], [DateReserved], [Confirmed], [Paid], [Claimed]) VALUES ('" + User.Identity.Name.ToString() + "', " + ((HyperLink)e.Item.FindControl("InventoryNoLink")).Text + ", '" + DateTime.Today.ToShortDateString() + "', 'False', 'False', 'False')";
                SqlDataSource2.Insert();

                ErrorMessageLabel.Visible = false;
            }
            else
            {
                ErrorMessageLabel.Visible = true;
            }
        }
        else if (((Button)e.CommandSource).Text == "More Info / Options")
        {
            Server.Transfer("Page_Vehicles.aspx?VehicleNumber=" + ((HyperLink)e.Item.FindControl("InventoryNoLink")).Text);
        }

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Server.Transfer("Page_Contractor.aspx?ContractorNumber=" + Eval("ContractorNumber"));
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

    }
    protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
