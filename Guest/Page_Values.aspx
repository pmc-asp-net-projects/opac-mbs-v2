<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_Values.aspx.cs" Inherits="Page_Values" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><img src="../Images/Event3.gif" /></center><br />
    <span style="font-size: 10pt; font-family: Tahoma"></span>
    <div>
        <font color="#339966" face="Arial" style="font-size: 8pt"><b><u>
            <br />
            <span style="font-size: 9pt; color: #000000;">Corporate Vision</span></u></b></font></div>
    <div>
        <font color="#339966" face="Arial" style="font-size: 8pt"></font>
        <ul>
            <li><font color="#339966" face="Arial" style="font-size: 8pt"><span style="color: #000000; font-size: 9pt;">
                GLC is the most admired
                brand in the industry. We are the first choice because we provide products and services
                that respond to the diverse needs of our various customers. 
            </span></font></li>
        </ul>
    </div>
    <div>
        <ul>
            <li><font color="#339966" face="Arial" style="font-size: 8pt">
                <br />
                <span style="color: #000000; font-size: 9pt;">
                We draw strength from Filipino craftsmanship, passion for quality and continuous
                improvement, professionalism, and teamwork.</span></font></li></ul>
        <font color="#339966" face="Arial" style="font-size: 8pt"></font>
    </div>
    <div>
        <font color="#339966" face="Arial" style="font-size: 8pt"></font>&nbsp;</div>
    <div>
        <font color="#339966" face="Arial" style="font-size: 8pt"><b><u>
            <br />
            <span style="font-size: 9pt; color: #000000;">Corporate Values</span></u></b></font></div>
    <div>
        <font color="#339966" face="Arial" style="font-size: 8pt"></font>
        <ul>
            <li><font color="#339966" face="Arial" style="font-size: 8pt"><span style="color: #000000; font-size: 9pt;">
                Passion for quality and
                continuous improvement</span></font></li></ul>
    </div>
    <div>
        <ul>
            <li><font color="#339966" face="Arial" style="font-size: 8pt"><span style="color: #000000; font-size: 9pt;">
                Genuine respect and concern
                for all our customers and stakeholders</span></font></li></ul>
    </div>
    <div>
        <ul>
            <li><font color="#339966" face="Arial" style="font-size: 8pt"><span style="color: #000000; font-size: 9pt;">
                Pride in ourselves, our
                company, and what we do</span></font></li></ul>
    </div>
    <div>
        <ul>
            <li><font color="#339966" face="Arial" style="font-size: 8pt"><span style="color: #000000; font-size: 9pt;">
                Working and having fun
                together as a God-centered family</span></font></li></ul>
        <font color="#339966" face="Arial" style="font-size: 8pt"></font>
    </div>
    <div>
        <font color="#339966" face="Arial" style="font-size: 8pt"></font>&nbsp;</div>
    <div>
        <font color="#339966" face="Arial" style="font-size: 8pt"><b><u>
            <br />
            <span style="font-size: 9pt; color: #000000;">Strategic Directions: BEST</span></u></b></font></div>
    <div>
        <font color="#339966" face="Arial" style="font-size: 8pt"><b></b></font>
        <ul>
            <li>
                <span style="color: #000000"><span style="font-size: 9pt"><font color="#000000" face="Arial" style="font-size: 8pt"><b>B</b></font><font
                color="#000000" face="Arial" style="font-size: 8pt">uild GLC as a brand.</font></span></span></li>
        </ul>
    </div>
    <div>
        <ul>
            <li>
                <span style="color: #000000"><span style="font-size: 9pt"><font color="#000000" face="Arial" style="font-size: 8pt"><b>E</b></font><font
                color="#000000" face="Arial" style="font-size: 8pt">xpand and enrich our markets</font></span></span></li>
        </ul>
    </div>
    <div>
        <ul>
            <li>
                <span style="color: #000000"><span style="font-size: 9pt"><font color="#000000" face="Arial" style="font-size: 8pt"><b>S</b></font><font
                color="#000000" face="Arial" style="font-size: 8pt">trengthen organizational capability
                and processes</font></span></span></li>
        </ul>
    </div>
    <div>
        <ul>
            <li>
                <span style="color: #000000"><span style="font-size: 9pt"><font color="#000000" face="Arial" style="font-size: 8pt"><b>T</b></font><font
                color="#000000" face="Arial" style="font-size: 8pt">ransform people</font></span></span></li>
        </ul>
        <font color="#000000" face="Arial" style="font-size: 8pt"></font>
    </div>
</asp:Content>

