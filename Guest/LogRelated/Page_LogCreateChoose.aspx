<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_LogCreateChoose.aspx.cs" Inherits="Page_LogCreateChoose" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    Do you already have an existing customer account? If you already have an existing
    customer account acquired through face-to-face transactions, you will just update
    your customer account, to be able to log-in. If not, you'll need to create an account<br />
    <br />
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Guest/LogRelated/Page_LogCreateUpdate.aspx">Update my existing account.</asp:HyperLink><br />
    <br />
    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Guest/LogRelated/Page_LogCreate.aspx">Create new account.</asp:HyperLink>
</asp:Content>

