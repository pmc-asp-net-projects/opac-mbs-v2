using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Page_LogCreate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        TextBox TelephoneNumberTextBoxVar = (TextBox)CreateUserWizard02.ContentTemplateContainer.FindControl("TelephoneNumberTextBox");
        Label WaitToValidateLabelVar = (Label)CompleteWizardStep01.ContentTemplateContainer.FindControl("WaitToValidateLabel");
        Label CustomerNumberLabelVar = (Label)CompleteWizardStep01.ContentTemplateContainer.FindControl("CustomerNumberLabel");
        
        SqlDataSource SqlDataSource1Var = (SqlDataSource)CreateUserWizard02.ContentTemplateContainer.FindControl("SqlDataSource1");
        WaitToValidateLabelVar.Visible = false;
        
        if (TotalRecordsChecker(SqlDataSource1Var, "SELECT COUNT(*) AS [Total] FROM [ExtraInfo] WHERE [TelephoneNumber] = '" + TelephoneNumberTextBoxVar.Text + "'") != 0)
        {
            WaitToValidateLabelVar.Visible = true;
            Roles.AddUserToRole(CreateUserWizard1.UserName, "Validating Customer");
        }
        else
        {
            Roles.AddUserToRole(CreateUserWizard1.UserName, "New Customer");
        }

        TextBox UserNameTextBox = (TextBox)CreateUserWizard02.ContentTemplateContainer.FindControl("UserName");
        SqlDataSource DataSource = (SqlDataSource)CreateUserWizard02.ContentTemplateContainer.FindControl("SqlDataSource1");
        MembershipUser User = Membership.GetUser(UserNameTextBox.Text);
        object UserGUID = User.ProviderUserKey;

        DataSource.InsertParameters.Add("UserId", UserGUID.ToString());
        DataSource.InsertParameters.Add("DateCreated", DateTime.Now.ToShortDateString());
        DataSource.InsertParameters.Add("IPAddress", Request.ServerVariables["REMOTE_ADDR"]);
        DataSource.Insert();

        CustomerNumberLabelVar.Text = TotalRecordsChecker(SqlDataSource1Var, "SELECT [CustomerNumber] AS [Total] FROM [ExtraInfo] WHERE [UserID] = '" + UserGUID.ToString() + "'").ToString();

    }
    protected void CreateUserWizard1_CreatingUser(object sender, LoginCancelEventArgs e)
    {
        Label ErrorMessageLabelVar = (Label)CreateUserWizard02.ContentTemplateContainer.FindControl("ErrorMessageLabel");
        TextBox LicenseNumberTextBoxVar = (TextBox)CreateUserWizard02.ContentTemplateContainer.FindControl("LicenseNumberTextBox");
        TextBox TelephoneNumberTextBoxVar = (TextBox)CreateUserWizard02.ContentTemplateContainer.FindControl("TelephoneNumberTextBox");
        SqlDataSource SqlDataSource1Var = (SqlDataSource)CreateUserWizard02.ContentTemplateContainer.FindControl("SqlDataSource1");

        Label DuplicateLicenseNumberErrorPipLabelVar = (Label)CreateUserWizard02.ContentTemplateContainer.FindControl("DuplicateLicenseNumberLicenseNumberErrorPipLabel");
        Label DuplicateTelephoneNumberErrorPipLabelVar = (Label)CreateUserWizard02.ContentTemplateContainer.FindControl("DuplicateTelephoneNumberErrorPipLabel");
        Label TermsAndConditionErrorPipLabelVar = (Label)CreateUserWizard02.ContentTemplateContainer.FindControl("TermsAndConditionErrorPipLabel");

        CheckBox TermsAndConditionCheckBoxVar = (CheckBox)CreateUserWizard02.ContentTemplateContainer.FindControl("TermsAndConditionCheckBox");

        ErrorMessageLabelVar.Text = "";
        ErrorMessageLabelVar.Visible = false;
        DuplicateLicenseNumberErrorPipLabelVar.Visible = false;
        DuplicateTelephoneNumberErrorPipLabelVar.Visible = false;
        TermsAndConditionErrorPipLabelVar.Visible = false;


        if (TotalRecordsChecker(SqlDataSource1Var, "SELECT COUNT(*) AS [Total] FROM [ExtraInfo] WHERE [LicenseNumber] = '" + LicenseNumberTextBoxVar.Text + "'") != 0)
        {

            ErrorMessageLabelVar.Text += "* Duplicate license number.";
            ErrorMessageLabelVar.Visible = true;
            DuplicateLicenseNumberErrorPipLabelVar.Visible = true;

            e.Cancel = true;

            SqlDataSource2.InsertCommand = "INSERT INTO [Logs] (Source, Message, DateCreated) VALUES ('" + Request.ServerVariables["REMOTE_ADDR"] + "', 'Attempted to register existing license number: " + LicenseNumberTextBoxVar.Text + "', " + DateTime.Now.ToShortDateString() + ")";
            SqlDataSource2.Insert();
        }

        if (TotalRecordsChecker(SqlDataSource1Var, "SELECT COUNT(*) AS [Total] FROM [ExtraInfo] WHERE [TelephoneNumber] = '" + TelephoneNumberTextBoxVar.Text + "'") != 0)
        {
            Label DuplicateTelephoneNumberLabelVar = (Label)CreateUserWizard02.ContentTemplateContainer.FindControl("DuplicateTelephoneNumberLabel");
            CheckBox UnderValidationCheckBoxVar = (CheckBox)CreateUserWizard02.ContentTemplateContainer.FindControl("UnderValidationCheckBox");

            DuplicateTelephoneNumberErrorPipLabelVar.Visible = true;

            DuplicateTelephoneNumberLabelVar.Visible = true;
            UnderValidationCheckBoxVar.Visible = true;

            if (UnderValidationCheckBoxVar.Checked == false) e.Cancel = true;
        }

        if (TermsAndConditionCheckBoxVar.Checked == false)
        {
            ErrorMessageLabelVar.Text += "<br />* You must agree with the Terms and Condition.";
            ErrorMessageLabelVar.Visible = true;

            TermsAndConditionErrorPipLabelVar.Visible = true;
        }
    }
    
    public static object ValueOrNull(object value)
    {
        if (value == DBNull.Value)
        {
            return null;
        }
        else
        {
            return value;
        }
    }
    public static int TotalRecordsChecker(SqlDataSource datasource, string SearchString03)
    {

        datasource.SelectCommand = SearchString03;
        System.Data.DataView dv = ((System.Data.DataView)(datasource.Select(DataSourceSelectArguments.Empty)));

        if (ValueOrNull(dv) != null) { return (Int32.Parse(dv.Table.Rows[0]["Total"].ToString())); }
        else { return (0); }
    }
}
