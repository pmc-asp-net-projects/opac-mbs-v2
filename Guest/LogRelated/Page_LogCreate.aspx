<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_LogCreate.aspx.cs" Inherits="Page_LogCreate" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><asp:CreateUserWizard ID="CreateUserWizard1" runat="server" BackColor="#E3EAEB" BorderColor="#E6E2D8"
        BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" Width="351px" OnCreatedUser="CreateUserWizard1_CreatedUser" OnCreatingUser="CreateUserWizard1_CreatingUser" ContinueDestinationPageUrl="~/Default.aspx" CancelDestinationPageUrl="~/Default.aspx" FinishDestinationPageUrl="~/Default.aspx" LoginCreatedUser="False">
        <WizardSteps>
            <asp:CreateUserWizardStep ID="CreateUserWizard02" runat="server">
                <ContentTemplate>
                    <table border="0" style="font-size: 100%; font-family: Verdana" width="100%">
                        <tr>
                            <td align="center" colspan="2" style="font-weight: bold; color: white; background-color: #1c5e55">
                                Account Information</td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label></td>
                            <td style="width: 181px" align="left">
                                <asp:TextBox ID="UserName" runat="server" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                    ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label></td>
                            <td align="left" style="width: 181px">
                                <asp:TextBox ID="Password" runat="server" TextMode="Password" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label></td>
                            <td style="width: 181px" align="left">
                                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                                    ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."
                                    ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label></td>
                            <td style="width: 181px" align="left">
                                <asp:TextBox ID="Email" runat="server" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                                    ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="Question">Security Question:</asp:Label></td>
                            <td style="width: 181px" align="left">
                                <asp:TextBox ID="Question" runat="server" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="QuestionRequired" runat="server" ControlToValidate="Question"
                                    ErrorMessage="Security question is required." ToolTip="Security question is required."
                                    ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Security Answer:</asp:Label></td>
                            <td style="width: 181px" align="left">
                                <asp:TextBox ID="Answer" runat="server" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer"
                                    ErrorMessage="Security answer is required." ToolTip="Security answer is required."
                                    ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="font-weight: bold; color: white; background-color: #1c5e55" align="center">
                                Personal Information</td>
                        </tr>
                        <tr>
                            <td align="right" style="height: 15px">
                                <asp:Label ID="Label1" runat="server" Text="Full Name:"></asp:Label></td>
                            <td align="left" style="width: 181px; height: 15px">
                                <asp:TextBox ID="FullNameTextBox" runat="server" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="FullNameTextBox"
                                    ErrorMessage="Full Name is required." ToolTip="Full Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td align="right" style="height: 15px">
                                <asp:Label ID="LicenseNumberLabel" runat="server" Text="License Number:"></asp:Label></td>
                            <td style="height: 15px; width: 181px;" align="left">
                                <asp:TextBox ID="LicenseNumberTextBox" runat="server" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="LicenseNumberTextBox"
                                    ErrorMessage="License Number is required." ToolTip="License Number is required."
                                    ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator><asp:Label ID="DuplicateLicenseNumberLicenseNumberErrorPipLabel"
                                        runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="TelephoneNumberLabel" runat="server" Text="Telephone Number:"></asp:Label></td>
                            <td style="width: 181px" align="left">
                                <asp:TextBox ID="TelephoneNumberTextBox" runat="server" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TelephoneNumberTextBox"
                                    ErrorMessage="Telephone Numer is required." ToolTip="Telephone Numer is required."
                                    ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator><asp:Label ID="DuplicateTelephoneNumberErrorPipLabel"
                                        runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="AddressLabel" runat="server" Text="Address:"></asp:Label></td>
                            <td style="width: 181px" align="left">
                                <asp:TextBox ID="AddressTextBox" runat="server" Height="22px" Width="146px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="AddressTextBox"
                                    ErrorMessage="Address is required." ToolTip="Address is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:HyperLink ID="TermsAndConditionHyperLink" runat="server" NavigateUrl="~/Guest/Page_TermsAndConditions.aspx">Terms and Condition</asp:HyperLink></td>
                            <td align="left" style="width: 181px">
                                <asp:CheckBox ID="TermsAndConditionCheckBox" runat="server" Checked="True" Text="I Agree" />
                                <asp:Label ID="TermsAndConditionErrorPipLabel" runat="server" ForeColor="Red" Text="*"
                                    Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" style="color: red; height: 84px;">
                                <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal><asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                                    ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                                    ValidationGroup="CreateUserWizard1"></asp:CompareValidator><asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="CreateUserWizard1" />
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExtraInfoConnectionString %>"
                                    InsertCommand="INSERT INTO ExtraInfo(UserID, LicenseNumber, TelephoneNumber, Address, DateCreated, IPAddress, FullName) VALUES (@UserID, @LicenseNumber, @TelephoneNumber, @Address, @DateCreated, @IPAddress, @FullName)"
                                    SelectCommand="SELECT * FROM [ExtraInfo]">
                                    <InsertParameters>
                                        <asp:ControlParameter Name="LicenseNumber" Type="String" ControlID="LicenseNumberTextBox" PropertyName="Text"/>
                                        <asp:ControlParameter Name="TelephoneNumber" Type="String" ControlID="TelephoneNumberTextBox" PropertyName="Text"/>
                                        <asp:ControlParameter Name="Address" Type="String" ControlID="AddressTextBox" PropertyName="Text"/>
                                        <asp:ControlParameter Name="FullName" Type="String" ControlID="FullNameTextBox" PropertyName="Text"/>
                                    </InsertParameters>
                                </asp:SqlDataSource>
                                <asp:Label ID="ErrorMessageLabel" runat="server" Visible="False"></asp:Label><br />
                                <asp:Label ID="DuplicateTelephoneNumberLabel" runat="server" Text="* Duplicate telephone number.<br /> Would you like be under validation group first?"
                                    Visible="False" Width="212px"></asp:Label>
                                <asp:CheckBox ID="UnderValidationCheckBox" runat="server" Text="Yes" Visible="False" /></td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:CreateUserWizardStep>
            <asp:CompleteWizardStep ID="CompleteWizardStep01" runat="server">
                <ContentTemplate>
                    <table border="0" style="font-size: 100%; font-family: Verdana" width="100%">
                        <tr>
                            <td align="center" colspan="2" style="font-weight: bold; color: white; background-color: #1c5e55">
                                Complete</td>
                        </tr>
                        <tr>
                            <td>
                                Your customer number is:
                                <asp:Label ID="CustomerNumberLabel" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                Your account has been successfully created.</td>
                        </tr>
                        <tr>
                            <td style="color: red">
                                <asp:Label ID="WaitToValidateLabel" runat="server" Text="Please wait for about 1-2 days to validate you account." Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="ContinueButton" runat="server" BackColor="White" BorderColor="#C5BBAF"
                                    BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" CommandName="Continue"
                                    Font-Names="Verdana" ForeColor="#1C5E55" Text="Continue" ValidationGroup="CreateUserWizard1" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:CompleteWizardStep>
        </WizardSteps>
        <SideBarStyle BackColor="#1C5E55" Font-Size="0.9em" VerticalAlign="Top" />
        <TitleTextStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <SideBarButtonStyle ForeColor="White" />
        <NavigationButtonStyle BackColor="White" BorderColor="#C5BBAF" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" ForeColor="#1C5E55" />
        <HeaderStyle BackColor="#666666" BorderColor="#E6E2D8" BorderStyle="Solid" BorderWidth="2px"
            Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
        <CreateUserButtonStyle BackColor="White" BorderColor="#C5BBAF" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" ForeColor="#1C5E55" />
        <ContinueButtonStyle BackColor="White" BorderColor="#C5BBAF" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" ForeColor="#1C5E55" />
        <StepStyle BorderWidth="0px" />
    </asp:CreateUserWizard>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatabaseConnectionString1 %>"
            SelectCommand="SELECT * FROM [Logs]"></asp:SqlDataSource>
        &nbsp;
        &nbsp;&nbsp;</center>
</asp:Content>

