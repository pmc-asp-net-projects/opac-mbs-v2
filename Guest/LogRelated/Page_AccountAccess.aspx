<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page_AccountAccess.aspx.cs" Inherits="Guest_LogRelated_Page_AccountAccess" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<center>
    &nbsp;<span style="font-size: 16pt"><br />
    <span style="font-size: 16pt"><strong></span>Account Options</STRONG></span><br />
    <span style="font-size: 16pt">&nbsp;</span>&nbsp;<br />
    <asp:LoginView ID="LoginView1" runat="server">
        <LoggedInTemplate>
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Guest/LogRelated/Page_LogChange.aspx">Change Password</asp:HyperLink><br />
            <br />
            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Guest/LogRelated/Page_LogSignOut.aspx">Sign Out</asp:HyperLink>
        </LoggedInTemplate>
        <AnonymousTemplate>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Guest/LogRelated/Page_LogIn.aspx">Sign In</asp:HyperLink><br />
            <br />
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Guest/LogRelated/Page_LogCreateChoose.aspx">Sign Up</asp:HyperLink><br />
            <br />
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Guest/LogRelated/Page_LogForgot.aspx">Forgot Password</asp:HyperLink>
        </AnonymousTemplate>
    </asp:LoginView>
    </center>
</asp:Content>

